import "../styles/globals.css";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import NextNProgress from "nextjs-progressbar";
import Head from "next/head";
import { Provider } from "react-redux";
import { store } from "../src/redux/store";
import ScrollToTop from "react-scroll-to-top";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowCircleUp } from "@fortawesome/free-solid-svg-icons";
import { useScrollPosition } from "@n8tb1t/use-scroll-position";
import dynamic from "next/dynamic";
import "react-toastify/dist/ReactToastify.css";

import { ToastContainer } from "react-toastify";

import { useEffect } from "react";
// if (typeof window !== "undefined") {
//   window.onscroll =  ()=> {
//     scrollFunction();
//   };

//   function scrollFunction() {
//    console.log('first')
//   }
// }
// if (typeof window !== "undefined") {
//   window.addEventListener("scroll", scrollFunction);
// }
// const scrollFunction = () => {
//   console.log(" first");
// };

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    document.body.scrollTop = 0;
  }, [pageProps]);


  return (
    <>
      <Provider store={store}>
        <Head>
          <link rel="icon" href="/asanbtc.ico" />
        </Head>

        <NextNProgress height={7} options={{ showSpinner: false }} />
       
        <Component {...pageProps} />
      
      </Provider>
      <ToastContainer />
    </>
  );
}

export default MyApp;
