import Link from "next/link";
import persianNumber from "../tool/persianNumber/persianNumber";
import style from "./Course.module.css";
import Countdown from "react-countdown";
import daysLeft from "../tool/daysLeft/daysLeft";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";
import palceImg from "../../../public/assets/images/AsanBTC.png";
import { useEffect, useState } from "react";
import { notify } from "../tool/toast/toast";

const Course = ({ data }) => {
  const [offPrice, setOffPrice] = useState(0);
  const [days, setDays] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);
  function toFarsiNumber(n) {
    const farsiDigits = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

    return n
      .toString()
      .split("")
      .map((x) => farsiDigits[x])
      .join("");
  }
  useEffect(() => {
    if (daysLeft(data.endDate) > 0) {
      if (data.offCodePercent !== 0) {
        let result = ((100 - data.offCodePercent) * data.totalPrice) / 100;
        setOffPrice(result);
      }
      if (data.offCodeAmount !== 0) {
        setOffPrice(data.totalPrice - data.offCodeAmount);
      }
    } else {
      setOffPrice(0);
    }
    setInterval(() => {
      var d1 = new Date(); //"now"

      var d2 = new Date("2022/05/01"); // some date

      var diff = Math.abs(d1 - d2);
      function convertToDays(milliSeconds) {
        let days = Math.floor(milliSeconds / (86400 * 1000));
        milliSeconds -= days * (86400 * 1000);
        let hours = Math.floor(milliSeconds / (60 * 60 * 1000));
        milliSeconds -= hours * (60 * 60 * 1000);
        let minutes = Math.floor(milliSeconds / (60 * 1000));
        milliSeconds -= minutes * (60 * 1000);
        let Seconds = Math.floor(milliSeconds / 1000);

        setDays(days);
        setHours(hours);
        setMinutes(minutes);
        setSeconds(Seconds);
        return {
          days,
          hours,
          minutes,
          Seconds
        };
      }
      var r = convertToDays(diff);
      if (r.days < 10) {
        r.days = "0" + r.days;
        setDays(r.days);
      }
      if (r.hours < 10) {
        r.hours = "0" + r.hours;
        setHours(r.hours);
      }
      if (r.minutes < 10) {
        r.minutes = "0" + r.minutes;
        setMinutes(r.minutes);
      }
      if (r.Seconds < 10) {
        r.Seconds = "0" + r.Seconds;
        setSeconds(r.Seconds);
      }
    }, 1000);
  }, []);

  return (
    <>
      <Link
        href={
          data.isEnable === true ? `/buyCourse/${data.classID}` : "/asanamooz"
        }
      >
        <a
          className={
            data.isEnable === false
              ? `${style.item} ${style.disableCourse}`
              : style.item
          }
          onClick={()=>data.isEnable===false&&notify("دوره غیر فعال می باشد","warning")}
        >
          {/* <Image src={palceImg}/> */}
          <LazyLoadImage
            placeholderSrc={palceImg}
            alt="imageCourse"
            src={data.filePath}
            effect="blur"
          />

          <div>
            <div className={style.content}>
              <h4>{data.className}</h4>
              <div className={style.teacher}>
                <img src="/assets/images/teacher.png" />
                <span>
                  {data.teacherName} {data.teacherFamily}
                </span>
              </div>
              <div className={style.duration}>
                <img src="/assets/images/duration.png" />
                <span>مدت زمان کلاس: {persianNumber(data.duration)} ساعت</span>
              </div>
            </div>
            {offPrice !== 0 ? (
              <div>
                <div className={style.off}>
                  <div>
                    <span>{persianNumber(data.totalPrice)}تومان</span>
                    <span>
                      {persianNumber(offPrice)}
                      تومان
                    </span>
                  </div>
                  <div className={style.offRate}>
                    <img src="/assets/images/off.png" />
                    <div>
                      <span className={style.percent}>
                        {persianNumber(data.offCodePercent)}%
                      </span>
                      {/* <Countdown
                        date={Date.now() + daysLeft(data.endDate)}
                        autoStart={true}
                      /> */}
                      <div className={style.timer}>
                        <div className={style.counter}>
                          <span>{toFarsiNumber(days)}</span>
                        </div>
                        <div className={style.semicolon}>:</div>
                        <div className={style.counter}>
                          <span> {toFarsiNumber(hours)}</span>
                        </div>
                        <div className={style.semicolon}>:</div>
                        <div className={style.counter}>
                          <span> {toFarsiNumber(minutes)}</span>
                        </div>
                        <div className={style.semicolon}>:</div>
                        <div className={style.counter}>
                          <span> {toFarsiNumber(seconds)}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <div className={style.Courseprice}>
                <div className={style.price}>
                  {data.totalPrice === "0" ? (
                    <span>رایگان</span>
                  ) : (
                    <span>
                      {persianNumber(data.totalPrice).toLocaleString()} تومان
                    </span>
                  )}
                </div>
              </div>
            )}
          </div>
        </a>
      </Link>
    </>
  );
};

export default Course;
