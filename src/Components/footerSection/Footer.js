import React, { useContext, useEffect, useState } from "react";
import style from "./Footer.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPhone,
  faMapMarkerAlt,
  faInstagram,
  faPaperPlane,
  faEnvelope
} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import MainButton from "../tool/mainButton/MainButton";
import Getphone from "../getPhoneUser/Getphone";
import { useDispatch, useSelector } from "react-redux";
import { showPopup } from "../../redux/getphone/getphoneAction";
import { useRouter } from "next/router";

const Footer = () => {
  const router = useRouter();
  const getphone = useSelector((state) => state.stateGetPhone);

  // const [popup, setPopUp] = useState(false);
  const state = useSelector((state) => state.stateLogin);
  const dispatch = useDispatch();
  useEffect(() => {
    window.$crisp = [];
    window.CRISP_WEBSITE_ID = "448930c2-88fb-4cd7-9d1e-5a5084927352";
    (() => {
      const d = document;
      const s = d.createElement("script");
      s.src = "https://client.crisp.chat/l.js";
      s.async = 1;
      d.getElementsByTagName("body")[0].appendChild(s);
    })();
  }, []);

  const registerHandler = () => {
    if (state.loginStatus) {
      router.push({
        pathname: "/payment"
      });
    } else {
      dispatch(showPopup());
      // setPopUp(true);
    }
  };
  /* const CrispWithNoSSR = dynamic(() => import("../crisp/crisp"), {
    ssr: false
  });
 */
  return (
    <>
      <div className={style.footerwave}>
        <svg viewBox="0 0 500 150" preserveAspectRatio="none">
          <path d="M-1.98,66.60 C200.05,-17.28 318.56,126.80 500.00,49.98 L500.00,150.00 L0.00,150.00 Z"></path>
        </svg>
      </div>
      <div className={style.footer}>
        <div className="container-fluid pl-5 pr-5">
          <div className={`row pb-5 ${style.footerDesktop}`}>
            <div className={`col-md-3 ${style.namad} center`}>
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d202.38499524690803!2d51.305462590195646!3d35.74687381633627!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3fd0e313c92de00e!2sAsanBTC!5e0!3m2!1sen!2snl!4v1650104053196!5m2!1sen!2snl"></iframe>
            </div>
            <div className={`col-md-5 ${style.ContactUsfoot}`}>
              <div>
                {state.loginStatus === false && (
                  <div onClick={registerHandler}>
                    <MainButton text="ورود/ ثبت نام" />
                  </div>
                )}
                {getphone.show && <Getphone />}
                <div className={style.phonenumber}>
                  <FontAwesomeIcon icon={faPhone} />
                  <p lang="fa">
                    <a href="tel:02144895362" id="FooterPhoneNum">
                      ۰۲۱۴۴۸۹۵۳۶۲
                    </a>
                  </p>
                </div>
                <div className={style.addressfoot}>
                  <p id="Address">
                    <FontAwesomeIcon icon={faMapMarkerAlt} />
                    تهران - جنت آباد جنوبی - خیابان لاله شرقی - پلاک ۱۰۵ - واحد
                    ۱۷- آسان بیت کوین
                  </p>
                </div>
              </div>
            </div>
            <div className={`col-md-4  p-0 ${style.footermenu}`}>
              <ul>
                <li>فهرست</li>
                <li>
                  <a href="https://asanbtc.com/Contents">اخبار</a>
                </li>
                <li>
                  <Link href="/AsanAmoz">آکادمی</Link>
                </li>
                <li>
                  <a href="https://asanbtc.com/AllCoins">قیمت ارزها</a>
                </li>
                <li>
                  <a href="https://asanbtc.com/Exchanges">صرافی ها</a>
                </li>
                <li>
                  <a href="https://asanbtc.com/About">درباره ما</a>
                </li>
              </ul>
              <ul>
                <li>برگزیده ها</li>
                <li>
                  <a href="https://asanbtc.com/Appliaction">
                    اپلیکیشن آسان بیت کوین
                  </a>
                </li>
                <li>
                  <a href="https://asanbtc.com/GainCalculator">
                    محاسبه گر سود و زیان
                  </a>
                </li>
                <li>
                  <a href="https://asanbtc.com/Convertor">مبدل</a>
                </li>
                <li>
                  <a href="https://asanbtc.com/Contents?LableFilter=%D9%84%DB%8C%D8%B3%D8%AA%20%D8%AF%D8%B1%20%D8%B5%D8%B1%D8%A7%D9%81%DB%8C">
                    لیست در صرافی
                  </a>
                </li>

                <li>
                  <a href="https://asanbtc.com/Contents?LableFilter=%D8%AA%D9%88%D9%82%D9%81%20%D9%87%D8%A7">
                    توقف ها
                  </a>
                </li>
              </ul>
            </div>
          </div>

          <div className="row">
            <div className={style.bottomfooter}>
              <div className={style.footertext}>
                <p>
                  <span>طراحی و پیاده سازی توسط</span>
                  <a href="http://asiasoftgp.com/" className="for-gray">
                    <span> آسیا پرداز</span>
                  </a>
                </p>
                <p>
                  <span> تمامی حقوق و محتوا برای </span>
                  <a href="https://asanbtc.com/" className="for-gray">
                    آسان بیت کوین
                  </a>
                  <span> محفوظ می باشد .</span>
                </p>
              </div>

              <div className={style.socialmedifooter}>
                <a id="PhoneLinkFoot" href="tel:02144895362">
                  <FontAwesomeIcon icon={faPhone} />
                </a>
                <a
                  id={style.InstaLinkFoot}
                  href="https://www.instagram.com/AsanBTCcom"
                >
                  {/* <FontAwesomeIcon icon={faInstagram} /> */}
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    x="0px"
                    y="0px"
                    viewBox="0 0 32 32"
                    style={{ fill: "#000000" }}
                  >
                    <path d="M 11.46875 5 C 7.917969 5 5 7.914063 5 11.46875 L 5 20.53125 C 5 24.082031 7.914063 27 11.46875 27 L 20.53125 27 C 24.082031 27 27 24.085938 27 20.53125 L 27 11.46875 C 27 7.917969 24.085938 5 20.53125 5 Z M 11.46875 7 L 20.53125 7 C 23.003906 7 25 8.996094 25 11.46875 L 25 20.53125 C 25 23.003906 23.003906 25 20.53125 25 L 11.46875 25 C 8.996094 25 7 23.003906 7 20.53125 L 7 11.46875 C 7 8.996094 8.996094 7 11.46875 7 Z M 21.90625 9.1875 C 21.402344 9.1875 21 9.589844 21 10.09375 C 21 10.597656 21.402344 11 21.90625 11 C 22.410156 11 22.8125 10.597656 22.8125 10.09375 C 22.8125 9.589844 22.410156 9.1875 21.90625 9.1875 Z M 16 10 C 12.699219 10 10 12.699219 10 16 C 10 19.300781 12.699219 22 16 22 C 19.300781 22 22 19.300781 22 16 C 22 12.699219 19.300781 10 16 10 Z M 16 12 C 18.222656 12 20 13.777344 20 16 C 20 18.222656 18.222656 20 16 20 C 13.777344 20 12 18.222656 12 16 C 12 13.777344 13.777344 12 16 12 Z"></path>
                  </svg>
                </a>
                <a id="telegramLinkFoot" href="https://t.me/asanbitcoin">
                  <FontAwesomeIcon icon={faPaperPlane} />
                </a>
                <a id="EmailAddress" href="mailto:info@asanbtc.com">
                  <FontAwesomeIcon icon={faEnvelope} />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <CrispWithNoSSR /> */}
    </>
  );
};

export default Footer;
