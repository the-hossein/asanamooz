import React, { useEffect, useState } from "react";
import style from "./Getphone.module.css";
import MainButton from "../tool/mainButton/MainButton";
import GetCode from "./getCode/GetCode";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMobile,
  faMobileAlt,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import { callApi } from "../apis/callApi";
import { BASE_URL, signUp, support } from "../apis/urls";
import { useDispatch, useSelector } from "react-redux";
import { hidePopup, otpId } from "../../redux/getphone/getphoneAction";
import { statusLoadingFalse } from "../../redux/buycourse/buyCourseActions";
const Getphone = ({ setPopUp, pages, status, totalPrice }) => {
  const state = useSelector((state) => state.stateGetPhone);
  const dispatch = useDispatch();
  const [phoneNumber, setPhoneNumber] = useState(null);
  const [error, setError] = useState();
  const [errorText, setErrorText] = useState();
  // const [otpID, setOtpId] = useState();
  const [page, setPage] = useState();
  const [preLoad, setpreLoad] = useState(false);
  const closepopUp = (e) => {
    dispatch(statusLoadingFalse());
    dispatch(hidePopup());
  };
  const chngehndlr = (e) => {
    var
persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
arabicNumbers  = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g],
fixNumbers = function (str)
{
  if(typeof str === 'string')
  {
    for(var i=0; i<10; i++)
    {
      str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
    }
  }
  return str;
};
    setPhoneNumber(fixNumbers(e.target.value));
  };
  const submitPhone = () => {
    setpreLoad(true);
    var regex = /(\+98|0)?9\d{9}/;
    // const number = document.getElementById("phone").value;
    if (
      phoneNumber !== null &&
      phoneNumber.length == 11 &&
      regex.test(phoneNumber)
    ) {
      const signUpReq = async () => {
        const myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        const raw = JSON.stringify({
          methodname: 5,
          checkPhoneNumber: {
            phonenumber: `${phoneNumber}`
          }
        });

        const signup = await callApi(BASE_URL + signUp, raw, myHeaders, "POST");
        if (signup[1] === 200) {
          setError("success");
          dispatch(otpId(signup[0].data.status.otpId));

          setpreLoad(false);
        } else {
          setError("wait");
          setErrorText(signup[0].message.message);
          setpreLoad(false);
        }
      };
      signUpReq();
    } else {
      setError("invalid");
      setErrorText("لطفا شماره همراه خود را به درستی وارد کنید");
      setpreLoad(false);
    }
  };
  useEffect(() => {
    if (pages === "support") {
      setPage("support");
    } else {
      setPage("signUp");
    }
  }, []);
  const TypeNumber = (e) => {
    if (!e) {
      e = window.event;
    }

    var code = e.keyCode || e.which;

    if (!e.ctrlKey) {
      if (code > 31 && (code < 48 || code > 57) && (code < 96 || code > 105)) {
        e.preventDefault();
        return false;
      }
    }
    return true;
  };

  return (
    <>
      <div className={style.popupStyle}>
        <div>
          <div>
            <div className={style.closeWindow}>
              <FontAwesomeIcon icon={faTimes} onClick={closepopUp} />
            </div>
            {error === "success" ? (
              <GetCode
                phone={phoneNumber}
                setError={setError}
                pages={pages}
                status={status}
                totalPrice={totalPrice}
              />
            ) : (
              <div className={style.getphone}>
                <h4>شماره موبایل خود را وارد کنید</h4>
                <label className={style.content}>
                  شماره تلفن همراه
                  <div className="input-group masked-input">
                    <div className="input-group-prepend">
                      <span
                        className={`input-group-text ${style.inputgrouptext}`}
                      >
                        <FontAwesomeIcon icon={faMobileAlt} />
                      </span>
                    </div>
                    <input
                      type="text"
                      id="phone"
                      maxLength={11}
                      value={phoneNumber}
                      onChange={chngehndlr}
                      className={`form-control ${style.formcontrol}`}
                      placeholder="***0912"
                      autoFocus
                      onKeyDown={(e) => TypeNumber(e)}
                    />
                  </div>
                </label>
                {error === "invalid" ? (
                  <span className={style.error}>{errorText}</span>
                ) : (
                  error === "wait" && (
                    <span className={style.error}>{errorText}</span>
                  )
                )}
                {preLoad && (
                  <div
                    className={`d-flex justify-content-center m-3 ${style.loading}`}
                  >
                    {/* <FontAwesomeIcon icon={faTimes}className={style.cloasePopup} onClick={closepopUp} /> */}

                    <div className="spinner-border text-success" role="status">
                      <span className="sr-only"></span>
                    </div>
                  </div>
                )}

                <div className={style.submitPhone}>
                  <button onClick={submitPhone}>ادامه</button>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default Getphone;
