import React, { useContext, useEffect, useState } from "react";
import SecondlyButton from "../../tool/secondlyButton/SecondlyButton";
import { useRouter } from "next/dist/client/router";
import style from "./GetCode.module.css";
import Countdown from "react-countdown";
import { callApi } from "../../apis/callApi";
import {
  addBasket,
  BASE_URL,
  checkUserClass,
  getProfile,
  packageDetails,
  signUp,
  support
} from "../../apis/urls";
import LoginSuccess from "../loginSuccessfully/LoginSuccess";
import { useDispatch, useSelector } from "react-redux";
import {
  getphonePreloadFalse,
  getphonePreloadTrue,
  statusBuyCourse,
  statusLoadingTrue
} from "../../../redux/buycourse/buyCourseActions";
import {
  alldataUser,
  loginTrue,
  userLogined,
  userNotLogined
} from "../../../redux/login/loginActions";
import { notify } from "../../tool/toast/toast";
import { hidePopup } from "../../../redux/getphone/getphoneAction";
const GetCode = ({ phone, setError, pages, setPopUp, status, totalPrice }) => {
  const router = useRouter();
  const state = useSelector((state) => state.stateGetPhone);
  const stateCourse = useSelector((state) => state.stateBuyCourse);
  const stateLogin = useSelector((state) => state.stateLogin);
  const dispatch = useDispatch();
  const [code, setCode] = useState();
  // // const [start, setStart] = useState(true);
  const [message, setMessage] = useState();
  const [succes, setSucces] = useState(false);
  const [prelod, setPreload] = useState(false);
  const [EndTime, setEndTime] = useState(false);
  const [EndTimeText, setEndTimeText] = useState("00:60");
  const [Estimate, setEstimate] = useState(60);

  const UpdateTimer = () => {
    if (EndTime === false) {
      Estimate > 0 &&
        setTimeout(() => {
          setEstimate(Estimate - 1);
          setEndTimeText("00:" + Estimate);
        }, 1000);
    }
  };
  UpdateTimer();

  const chngehndlr = (e) => {
    var persianNumbers = [
        /۰/g,
        /۱/g,
        /۲/g,
        /۳/g,
        /۴/g,
        /۵/g,
        /۶/g,
        /۷/g,
        /۸/g,
        /۹/g
      ],
      arabicNumbers = [
        /٠/g,
        /١/g,
        /٢/g,
        /٣/g,
        /٤/g,
        /٥/g,
        /٦/g,
        /٧/g,
        /٨/g,
        /٩/g
      ],
      fixNumbers = function (str) {
        if (typeof str === "string") {
          for (var i = 0; i < 10; i++) {
            str = str
              .replace(persianNumbers[i], i)
              .replace(arabicNumbers[i], i);
          }
        }
        return str;
      };
    setCode(fixNumbers(e.target.value));
  };
  const backhndler = () => {
    setError();
  };
  const sendAgain = () => {
    setEstimate(60);
    setEndTimeText("00:60");
    // setStart(true);
    const signUpReq = async () => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify({
        methodname: 5,
        checkPhoneNumber: {
          phonenumber: `${phone}`
        }
      });

      const signup = await callApi(BASE_URL + signUp, raw, myHeaders, "POST");
      return signup;
    };
    signUpReq();
  };
  // const endTime = () => {
  //   setStart(false);
  // };
  const submitCode = () => {
    setPreload(true);

    let typeSignUp = 1;
    if (state.statusphone === "support") {
      typeSignUp = 2;
    }

    const signupCheck = async () => {
      var raw = JSON.stringify({
        methodname: 2,
        otpCheck: {
          phoneNumber: `${phone}`,
          code: `${code}`,
          type: typeSignUp
        }
      });

      const sign = await callApi(
        BASE_URL + signUp,
        raw,
        { "Content-Type": "application/json" },
        "POST"
      );
      console.log(raw);
      if (sign[0].code === 200 || sign[0].code === 201) {
        // dispatch(getProfile(phone))
        console.log(sign);
        dispatch(getphonePreloadTrue());

        setMessage(sign[0].message);
        console.log(sign[0].message);
        if (sign[0].message === "با موفقیت لاگین شدید ") {
          dispatch(hidePopup());
        } else {
          notify(`${sign[0].message}`, "success");
          dispatch(hidePopup());
        }
        setSucces(true);
        const data = {
          token: sign[0].data.token,
          exp: sign[0].data.expiration,
          phone: phone
        };
        localStorage.setItem("userToken", JSON.stringify(data));
        dispatch(alldataUser());
        //  await callApi(
        //     `${BASE_URL + support}?OtpId=${sign[0].data.otpId}&phoneNumber=${phoneNumber}`,
        //     "{}",
        //     { "Content-Type": "application/json" },
        //     "POST"
        //   );

        setPreload(false);
        dispatch(getphonePreloadFalse());

        if (state.statusphone === "buy") {
          dispatch(statusLoadingTrue());

          var myHeaders = new Headers();
          myHeaders.append("Authorization", `Bearer ${sign[0].data.token}`);
          myHeaders.append("Content-Type", "application/json");
          let userProfile = async () => {
            var raw = JSON.stringify({
              methodname: 3,
              loginWithPassword: {
                userName: phone
              }
            });

            const profile = await callApi(
              BASE_URL + getProfile,
              raw,
              myHeaders,
              "POST"
            );
            dispatch(statusLoadingTrue());

            if (profile[0].code === 200 && profile[0].data !== null) {
              var userId = profile[0].data.id;
              dispatch(statusLoadingTrue());

              const PackageDeatil = async () => {
                const deatils = await callApi(
                  `${BASE_URL + packageDetails}?id=${stateCourse.data.id}`,
                  "{}",
                  { "Content-Type": "application/json" },
                  "GET"
                );

                dispatch(statusLoadingTrue());
                if (deatils[0].code === 200) {
                  dispatch(statusLoadingTrue());

                  const AddBasket = async () => {
                    const raw = JSON.stringify({
                      userid: userId,
                      basket_title: `${stateCourse.data.title}خرید دوره`,
                      phone_number: phone,
                      description: `${stateCourse.data.title}خرید دوره`,
                      qty: 1,
                      amount: totalPrice,
                      package_detail: deatils[0].data
                    });

                    console.log("first", raw);
                    const paymentData = await callApi(
                      BASE_URL + addBasket,

                      raw,
                      myHeaders,
                      "POST"
                    );
                    console.log(paymentData);

                    if (
                      paymentData[0].code === 200 &&
                      paymentData[0].data !== null
                    ) {
                      console.log("paymentData", stateCourse);

                      let checkClass = async () => {
                        let classStatus = await callApi(
                          `${
                            BASE_URL + checkUserClass
                          }?UserId=${userId}&ClassId=${stateCourse.data.id}`,
                          "{}",
                          myHeaders,
                          "GET"
                        );
                        if (classStatus[0].data === false) {
                          router.push({
                            pathname: "/payment"
                          });
                        } else {
                          dispatch(statusBuyCourse("bought"));
                        }
                      };
                      checkClass();
                    }
                  };
                  AddBasket();
                }
              };
              PackageDeatil();
            }
          };
          userProfile();
        }
      } else {
        setMessage("کد وارد شده اشتباه است");
        dispatch(userNotLogined());
        setPreload(false);
      }
    };
    signupCheck();
  };
  const TypeNumber = (e) => {
    if (!e) {
      e = window.event;
    }

    var code = e.keyCode || e.which;

    if (!e.ctrlKey) {
      if (code > 31 && (code < 48 || code > 57) && (code < 96 || code > 105)) {
        e.preventDefault();
        return false;
      }
    }
    return true;
  };
  return (
    <>
      <div className={style.getCode}>
        <div>
          <p className={style.text}>
            کد به شماره
            <span className={style.phonenum}>{phone}</span>
            پیامک شد
          </p>
          <div>
            <input
              type="text"
              id="code"
              value={code}
              onChange={chngehndlr}
              className={`form-control ${style.formcontrol}`}
              maxLength={6}
              placeholder="لطفا کد ارسالی را وارد نمایید"
              autoFocus
              onKeyDown={(e) => TypeNumber(e)}
            />
            <span className={style.error}>{message}</span>
            <div className={style.allbuttons}>
              <button onClick={submitCode}>
                <span>ثبت</span>

                {prelod && (
                  <div
                    className={`spinner-border text-light ${style.sppiner}`}
                    role="status"
                  >
                    <span className="sr-only"></span>
                  </div>
                )}
              </button>

              <button>
                {/* <span className={start ? style.notShow : style.show}>
                    ارسال مجدد
                  </span> */}
                {Estimate === 0 ? (
                  <span onClick={sendAgain}>ارسال مجدد</span>
                ) : (
                  EndTimeText
                )}
              </button>
            </div>
            <button className={style.changePhone} onClick={backhndler}>
              تغییر شماره همراه
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default GetCode;
