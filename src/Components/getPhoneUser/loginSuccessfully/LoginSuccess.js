import React from "react";
import { useDispatch } from "react-redux";
import { hidePopup } from "../../../redux/getphone/getphoneAction";
import style from "./LoginSuccess.module.css";
const LoginSuccess = ({ page, message, setPopUp }) => {
  const dispatch=useDispatch()
  if (message === "با موفقیت لاگین شدید ") {
 dispatch(hidePopup())
  }
  return (
    <>
      <div>
        {page === "support" ? (
          <span className={style.content}>درخواست مشاوره شما ثبت شد</span>
        ) : (
          <span className={style.content}>{message}</span>
        )}
      </div>
    </>
  );
};

export default LoginSuccess;
