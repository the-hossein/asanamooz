import * as Y from "yup";

export const isValidIranianNationalCode = (input) => {
  if (!/^\d{10}$/.test(input)) return false;

  if (/^(\d)\1+$/.test(input)) return false;

  let check = +input[9];
  let sum =
    Array(9)
      .fill()
      .map((_, i) => +input[i] * (10 - i))
      .reduce((x, y) => x + y) % 11;
  return (sum < 2 && check === sum) || (sum >= 2 && check + sum === 11);
};

export const mobile = Y.string()
  .trim()
  .required("شماره موبایل الزامی است.")
  .matches(/^[0-9]+$/, "فقط عدد وارد کنید.")
  .length(11, "طول شماره موبایل باید 11 رقم باشد");
