import React, { useState, useLayoutEffect, useEffect, useContext } from "react";
import style from "./Header.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faCaretDown,
  faNewspaper,
  faVolumeUp,
  faChartBar,
  faMicrophone,
  faUsers,
  faGavel,
  faPhone,
  faBars,
  faQuestionCircle,
  faReply,
  faUserCheck,
  faShoppingCart,
  faSearch,
  faChevronDown,
  faBookmark,
  faSignOutAlt
} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import Search from "../tool/serachBox/Search";
import MenuBar from "../../MobileComponents/burgerMenu/BergurMenu";
import MainButton from "../tool/mainButton/MainButton";
import Getphone from "../getPhoneUser/Getphone";
import { useRouter } from "next/router";
import { BASE_URL, getBasket, getProfile } from "../apis/urls";
import { callApi } from "../apis/callApi";
import Image from "next/image";
import asanBtc from "../../../public/assets/images/AsanBTC.png";
import { useDispatch, useSelector } from "react-redux";
import {
  alldataUser,
  getuserId,
  phoneNumber,
  userLogined,
  userNotLogined
} from "../../redux/login/loginActions";

import { showPopup } from "../../redux/getphone/getphoneAction";
import persianNumber from "../tool/persianNumber/persianNumber";
const Header = () => {
  const router = useRouter();
  const [size, setSize] = useState([0]);
  const [showMenu, setShowmenu] = useState(false);
  const [searchBox, setSearchBox] = useState(false);
  const [fullBasket, setFullBasket] = useState();
  // const [popup, setPopUp] = useState(false);
  const [userCourse, setUserCourse] = useState(false);
  const [userName, setUserName] = useState();
  const state = useSelector((state) => state.stateLogin);
  const getphone = useSelector((state) => state.stateGetPhone);
  /*  const UserData = useSelector((state) => state.stateLogin.userData); */
  const dispatch = useDispatch();
  if (typeof window !== "undefined") {
    var ls = localStorage.getItem("userToken");
  }
  const registerHandler = () => {
    dispatch(showPopup());
  };
  const showSubmenu = (e) => {
    const show = document.getElementsByClassName(style.show);
    for (let i = 0; i < show.length; i++) {
      show[i].classList.remove(style.show);
    }
    if (e.target.getAttribute("id") === "news") {
      document.getElementById("subitemNews").classList.toggle(style.show);
    } else if (e.target.getAttribute("id") === "abouUs") {
      document.getElementById("subitemAboutUs").classList.toggle(style.show);
    }
  };
  const closeSubmenu = () => {
    const show = document.getElementsByClassName(style.show);
    for (let i = 0; i < show.length; i++) {
      show[i].classList.remove(style.show);
    }
  };
  const seachhndler = () => {
    setSearchBox(true);
  };
  const showMobileMenu = () => {
    setShowmenu(!showMenu);
  };
  const backPage = () => {
   
    window.history.go(-1);
  };

  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth]);
    }
    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);

  useEffect(() => {
    if (ls) {
      if (state.loginStatus === false && state.userData.length !== 0) {
        dispatch(alldataUser());

        const userToken = JSON.parse(ls);
        var phone = userToken.phone;
        var token = userToken.token;

        const tokenExp = userToken.exp;
        const now = new Date();
        const endDate = new Date(tokenExp);
        if (endDate - now < 0) {
          localStorage.removeItem("userToken");
          dispatch(userNotLogined());
        }
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${token}`);
        myHeaders.append("Content-Type", "application/json");
      }
    } else {
      dispatch(userNotLogined());
    }
  }, []);
  const carHandler = () => {
    if (state.loginStatus) {
      router.push({
        pathname: "/payment"
      });
    } else {
      dispatch(showPopup());
      // setPopUp(true);
    }
  };
  const showUserClassButton = () => {
    setUserCourse(!userCourse);
  };
  const logout = () => {
    localStorage.removeItem("userToken");
    location.reload();
  };
  return (
    <>
      {getphone.show && <Getphone />}
      {size > 768 ? (
        <div className={style.container}>
          <div className={style.menu}>
            <div>
              {state.loginStatus ? (
                <div className={style.userLogin} onClick={showUserClassButton}>
                  <FontAwesomeIcon icon={faUserCheck} />
                  {state.loadingState ? null : (
                    <span>
                      {state.userData.name !== "نامشخص" ||
                      state.userData.family !== "نامشخص"
                        ? state.userData.name + " " + state.userData.family
                        : state.userData.phonenumber}
                      {/* {state.phoneNumber} */}
                    </span>
                  )}

                  <FontAwesomeIcon
                    icon={faChevronDown}
                    className={style.arrowDown}
                  />
                  {userCourse && (
                    <>
                      <ul className={style.yourCourse}>
                        <Link href="/userCourses">
                          <li>
                            <FontAwesomeIcon icon={faBookmark} />
                            دوره های شما
                          </li>
                        </Link>
                        <li onClick={logout}>
                          <FontAwesomeIcon icon={faSignOutAlt} />
                          خروج
                        </li>
                      </ul>
                    </>
                  )}
                </div>
              ) : (
                <div onClick={registerHandler}>
                  <MainButton text="ورود/ ثبت نام" />
                </div>
              )}
              <ul className={style.menuitems}>
                <li>
                  <a href="https://asanbtc.com/">
                    <FontAwesomeIcon icon={faHome} />
                  </a>
                </li>
                <li
                  onMouseLeave={closeSubmenu}
                  onMouseEnter={showSubmenu}
                  onClick={showSubmenu}
                >
                  <a id="news">
                    اخبار <FontAwesomeIcon icon={faCaretDown} />
                  </a>
                  <div id="subitemNews" className={style.subItem}>
                    <a href="https://asanbtc.com/Contents?typeid=1#MainContent_UpdateContent">
                      <FontAwesomeIcon icon={faVolumeUp} />
                      اخبار
                    </a>
                    <a href="https://asanbtc.com/Contents?typeid=3#MainContent_UpdateContent">
                      <FontAwesomeIcon icon={faChartBar} />
                      تحلیل
                    </a>
                    <a href="https://asanbtc.com/Contents?typeid=2#MainContent_UpdateContent">
                      <FontAwesomeIcon icon={faNewspaper} />
                      مقاله
                    </a>
                    <a href="https://asanbtc.com/Contents?typeid=4#MainContent_UpdateContent">
                      <FontAwesomeIcon icon={faMicrophone} />
                      مصاحبه
                    </a>
                  </div>
                </li>
                <li>
                  <a href="https://asanbtc.com/AllCoins">قیمت ارزها</a>
                </li>
                <li>
                  <a href="https://asanbtc.com/Exchanges">صرافی ها</a>
                </li>
                <li>
                  <a href="https://asanbtc.com/AllCoins">خرید و فروش</a>
                </li>
                <li>
                  <Link href="/asanamooz">
                    <a className={style.asanAmoz}>آسان آموز</a>
                  </Link>
                </li>
                {/* <li>
                  <a href="https://asanbtc.com/Consulting">مشاوره</a>
                </li> */}
                <li onMouseLeave={closeSubmenu} onMouseEnter={showSubmenu}>
                  <a id="abouUs">
                    درباره ما <FontAwesomeIcon icon={faCaretDown} />
                  </a>
                  <div id="subitemAboutUs" className={style.subItem}>
                    <a href="https://asanbtc.com/About">
                      <FontAwesomeIcon icon={(faUsers, faUsers)} />
                      تیم ما
                    </a>
                    <a href="https://asanbtc.com/About#rules">
                      <FontAwesomeIcon icon={faGavel} />
                      قوانین ما
                    </a>
                    <a href="https://asanbtc.com/About#ContactUs">
                      <FontAwesomeIcon icon={faPhone} />
                      ارتباط با ما
                    </a>
                    <a href="https://asanbtc.com/FAQ">
                      <FontAwesomeIcon icon={faQuestionCircle} />
                      سوالات متداول
                    </a>
                  </div>
                </li>
                <li>
                  <a href="https://asanbtc.com/Service">خدمات ما</a>
                </li>
              </ul>
            </div>
          </div>

          <div className={style.icoins}>
            <div className={style.basketcount}>
              <img
                src="/assets/images/cart.png"
                alt="cartIcon"
                onClick={carHandler}
              />

              <span className={state.basketInventory && style.count}></span>
            </div>
            {/* <img
              src="/assets/images/search.png"
              alt="searchicon"
              onClick={seachhndler}
            /> */}

            {/* <img src="/assets/images/AsanBTC.png" alt="logo" /> */}
          <Link href="/">

            <div className={style.asan}>
              <Image src={asanBtc} alt="logo" />
            </div>
           </Link>
          </div>

          {/* {searchBox && <Search setSearchBox={setSearchBox} />} */}
        </div>
      ) : (
        <div className={style.hamburgerMenu}>
          <FontAwesomeIcon icon={faBars} onClick={showMobileMenu} />
          {/* <img src="/assets/images/AsanBTC.png" alt="logo" /> */}
          <Link href="/">

          <Image src={asanBtc} alt="logo" />
          
          </Link>
          <FontAwesomeIcon
            icon={faReply}
            className={style.undo}
            onClick={backPage}
          />

          {showMenu && <MenuBar setShowmenu={setShowmenu} />}
        </div>
      )}
    </>
  );
};

export default Header;
