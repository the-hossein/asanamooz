import React, { useState, useLayoutEffect, useEffect } from "react";
import Steper from "../../tool/steperTool/Steper";
import Courses from "./courses/Courses";
import Filter from "./filter/Filter";
import style from "./AsanAmoz.module.css";
import { Spliter } from "../../tool/spliter/Spliter";
import ConsultationRequest from "../landing/consultationRequest/ConsultationRequest";
import MobileCourses from "../../../MobileComponents/mobileCourses/MobileCourses";
import { callApi } from "../../apis/callApi";
import { BASE_URL, getAllCoursesRequest } from "../../apis/urls";
import Loader from "../../tool/loader/Loader";
import { useDispatch, useSelector } from "react-redux";
import { getAllCourses } from "../../../redux/asanamooz/asanamoozActions";
const AsanAmoz = () => {
  const [preLoad, setPreLoad] = useState(true);
  const [size, setSize] = useState([0]);
  const [tabActive, setTabActive] = useState("all");
  const [teachrname, setTeachrname] = useState(null);
  const [levelcourse, setLevelcourse] = useState(null);
  const [priceCourse, setPriceCourse] = useState(null);
  const [off, setOff] = useState();
  const [free, setFree] = useState();
  const state = useSelector((state) => state.stateAsanAmoz);
  const dispatch = useDispatch();
  useEffect(() => {
    if (!state.allCourses.length) {
      const getdatas = async () => {
        const getdatas = await callApi(
          BASE_URL + getAllCoursesRequest,
          "{}",
          { "Content-Type": "application/json" },
          "GET"
        );
        const coursesData = getdatas[0].data;
        dispatch(getAllCourses(coursesData));
      };

      return getdatas();
    }
  }, []);
  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth]);
    }
    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);

  return (
    <>
      {size > 768 ? (
        <div className="container-fluid mt-5 mb-5">
          <div className={`row flexcenter `}>
            <div className={`col-lg-10 col-md-11 col-12`}>
              <div className="row">
                <div className={`col-lg-3 col-3 `}>
                  <Filter
                    setTeachrname={setTeachrname}
                    setLevelcourse={setLevelcourse}
                    setPriceCourse={setPriceCourse}
                  />
                </div>
                <div className="col-lg-9 col-9">
                  {state.loading ? (
                    <Loader />
                  ) : (
                    <Courses
                      tabActive={tabActive}
                      setTabActive={setTabActive}
                      teachrname={teachrname}
                      levelcourse={levelcourse}
                      priceCourse={priceCourse}
                      setLevelcourse={setLevelcourse}
                      off={off}
                      setOff={setOff}
                      free={free}
                      setFree={setFree}
                    />
                  )}
                </div>
              </div>
              <Spliter textSpliter="در خواست مشاوره" />
              <ConsultationRequest />
            </div>

            <Steper activestep={2} />
          </div>
        </div>
      ) : state.loading ? (
        <Loader />
      ) : (
        <MobileCourses />
      )}
    </>
  );
};

export default AsanAmoz;
