import React, { useEffect, useState } from "react";
import style from "./Courses.module.css";
import Paginate from "../../../tool/paginates/Paginate";
import { useSelector } from "react-redux";

const Courses = ({
  levelcourse,
  priceCourse,
  setLevelcourse,
  tabActive,
  setTabActive
}) => {
  const state = useSelector((state) => state.stateAsanAmoz);
  const [filterData, setFilterData] = useState(state.allCourses);
  const [filteritem, setFilteritem] = useState(filterData);

  useEffect(() => {
    // if (tabActive === "all") {
    // setFilteritem(filterData);
    if (levelcourse !== null) {
      setFilteritem(
        filterData.filter((item) => {
          if (levelcourse === "all") {
            return item;
          } else {
            return item.courseLevel === levelcourse;
          }
        })
      );
    }
    if (priceCourse !== null) {
      setFilteritem(
        filterData.filter((item) => {
          if (priceCourse === "all") {
            return item;
          } else {
            if (priceCourse === "off") {
              return item.offCodeAmount !== 0;
            }
            if (priceCourse === "0") {
              return item.totalPrice === 0;
            }
          }
        })
      );
    }
    // if (teachrname !== null) {
    //   setFilteritem(
    //     filterData.filter((item) => item.teacherFamily === teachrname)
    //   );
    //   if (teachrname === "all") {
    //     setFilteritem(filterData);
    //   }
    // }
    if (levelcourse !== null && priceCourse !== null) {
      setFilteritem(
        filterData.filter((item) => {
          if (levelcourse === "all" && priceCourse === "all") {
            return item;
          }
          if (levelcourse === "all" && priceCourse === "off") {
            return item.offCodeAmount !== 0;
          }
          if (levelcourse === "all" && priceCourse === "free") {
            return item.totalPrice === 0;
          }
          if (levelcourse !== null && priceCourse === "off") {
            return item.courseLevel === levelcourse && item.offCodeAmount !== 0;
          }
          if (levelcourse !== null && priceCourse === "free") {
            return item.courseLevel === levelcourse && item.totalPrice === 0;
          }
          if (levelcourse !== null && priceCourse === "all") {
            return item.courseLevel === levelcourse;
          }
        })
      );
    }
    //}

    // if (tabActive === "beg") {
    //   setFilteritem(filterData.filter((item) => item.courseLevel === "beg"));
    //   if (levelcourse !== null) {
    //     if (levelcourse === "pro") {
    //       setTabActive("pro");
    //     }
    //     setFilteritem(
    //       filterData.filter((item) => item.courseLevel === levelcourse)
    //     );
    //     if (levelcourse === "all") {
    //       setTabActive("all");
    //       setFilteritem(filterData);
    //     }
    //   }
    // }

    // if (tabActive === "pro") {
    //   setFilteritem(filterData.filter((item) => item.courseLevel === "pro"));
    //   if (levelcourse !== null) {
    //     if (levelcourse === "beg") {
    //       setTabActive("beg");
    //     }
    //     setFilteritem(
    //       filterData.filter((item) => item.courseLevel === levelcourse)
    //     );
    //     if (levelcourse === "all") {
    //       setTabActive("all");
    //       setFilteritem(filterData);
    //     }
    //   }
    // }
    // if (tabActive === "free") {
    //   setFilteritem(filterData.filter((item) => item.price === "0"));
    // }
    // if (tabActive === "off") {
    //   setFilteritem(filterData.filter((item) => item.offCodeAmount !== 0));
    // }

  
  }, [levelcourse, priceCourse]);

  // const Begginercourse = (e) => {
  //   setTabActive("beg");
  //   document.getElementById("beg").checked = true;
  //   setLevelcourse("beg");
  // };

  const All = (e) => {
    setTabActive(e.target.getAttribute("name"));
    setLevelcourse("all");
  };

  // const Advanced = (e) => {
  //   setTabActive(e.target.getAttribute("name"));

  //   document.getElementById("pro").checked = true;
  //   setLevelcourse("pro");
  // };
  // const Free = (e) => {
  //   setTabActive(e.target.getAttribute("name"));
  //   setLevelcourse("free");
  // };
  // const Off = (e) => {
  //   setTabActive(e.target.getAttribute("name"));
  //   setLevelcourse("off");
  // };
  return (
    <>
      <div className={style.container}>
    
        <div className={`${style.tabs} mb-3`}>
          <div
            className={tabActive === "all" && style.active}
            name="all"
            onClick={All}
          >
            همه
          </div>
          {/* <div
            className={tabActive === "beg" && style.active}
            name="beg"
            onClick={Begginercourse}
          >
            دوره مبتدی
          </div>
          <div
            className={tabActive === "pro" && style.active}
            name="pro"
            onClick={Advanced}
          >
            دوره پیشرفته
          </div>
          <div
            className={tabActive === "free" && style.active}
            name="free"
            onClick={Free}
          >
            دوره رایگان
          </div>
          <div
            className={tabActive === "off" && style.active}
            name="off"
            onClick={Off}
          >
            تخفیف ویژه
          </div> */}
        </div>
        <Paginate perpage={8} datas={filteritem.sort((a, b) => Number(b.isEnable) - Number(a.isEnable))} />
      </div>
    </>
  );
};

export default Courses;
