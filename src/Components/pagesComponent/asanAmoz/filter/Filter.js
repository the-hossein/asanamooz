import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect } from "react";
import ChosePort from "../../payment/chosePort/ChosePort";
import style from "./Filter.module.css";

const Filter = ({
  setTeachrname,

  setLevelcourse,
  setPriceCourse,


}) => {
  useEffect(() => {
    const radioitems = document.getElementsByClassName("form-check-input");
    for (let i = 0; i < radioitems.length; i++) {
      if (radioitems[i].getAttribute("value") === "all") {
        radioitems[i].checked = true;
      }
    }
  }, []);
  const radioHndler = (e) => {
    switch (e.target.name) {
      // case "teachrname":
      //   setTeachrname(e.target.value);
      //   break;
      case "levelcourse":
        setLevelcourse(e.target.value);

        break;
      case "priceCourse":
        setPriceCourse(e.target.value);

        break;
      default:
        break;
    }
    //  setRadioFilter({[e.target.name]:e.target.value})
  };
  // const openTeacherName = () => {
  //   const filtername = document.getElementById("byName");
  //   filtername.classList.toggle(style.show);
  //   filtername.previousElementSibling.firstElementChild.firstElementChild.classList.toggle(
  //     style.rotate
  //   );
  // };
  const openLevel = () => {
    const filterlevel = document.getElementById("bylevel");
    filterlevel.classList.toggle(style.show);
    filterlevel.previousElementSibling.firstElementChild.firstElementChild.classList.toggle(
      style.rotate
    );
  };
  const openPrice = () => {
    const filterPrice = document.getElementById("byprice");
    filterPrice.classList.toggle(style.show);
    filterPrice.previousElementSibling.firstElementChild.firstElementChild.classList.toggle(
      style.rotate
    );
  };

  return (
    <>
      <div className={`colStyle ${style.filterSticky}`}>
        <p className={style.title}> فیلتر ها</p>
        <div className={style.filters}>
          <ul>
            {/* <li onClick={openTeacherName}>
              <div className="name">
                نام مدرس
                <FontAwesomeIcon icon={faChevronDown} />
              </div>
            </li>
            <div className={style.subitem} id="byName">
              <label className="form-check-label">
                <input
                  onClick={radioHndler}
                  className="form-check-input"
                  name="teachrname"
                  type="radio"
                  value="all"
                  id="all"
                />
                همه
              </label>
              <label className="form-check-label">
                <input
                  onClick={radioHndler}
                  className="form-check-input"
                  name="teachrname"
                  type="radio"
                  value="احمدزادگان"
                  id="ahmadzadegan"
                />
                مهندس حمید احمدزادگان
              </label>
              <label className="form-check-label">
                <input
                  onClick={radioHndler}
                  className="form-check-input"
                  name="teachrname"
                  type="radio"
                  value="خاکبازان"
                  id="khakbazan"
                />
                مهندس مسعود خاکبازان
              </label>
            </div> */}
            <li onClick={openLevel} >
              <div className="level">
                سطح دوره
                <FontAwesomeIcon icon={faChevronDown} />
              </div>
            </li>
            <div className={`${style.subitem} ${style.show}`} id="bylevel" >
              <label className="form-check-label">
                <input
                  onClick={radioHndler}
                  className="form-check-input"
                  name="levelcourse"
                  type="radio"
                  value="all"
                  id="all"
                />
                همه
              </label>
              <label className="form-check-label">
                <input
                  onClick={radioHndler}
                  className="form-check-input"
                  name="levelcourse"
                  type="radio"
                  value="مقدماتی"
                  id="beg"
                />
                مقدماتی
              </label>
              <label className="form-check-label">
                <input
                  onClick={radioHndler}
                  className="form-check-input"
                  name="levelcourse"
                  type="radio"
                  value="پیشرفته"
                  id="pro"
                />
                پیشرفته
              </label>
            </div>
            {/* <li onClick={openPrice}>
              <div className="price">
                قیمت دوره
                <FontAwesomeIcon icon={faChevronDown} />
              </div>
            </li>
            <div className={style.subitem} id="byprice">
              <label className="form-check-label">
                <input
                  onClick={radioHndler}
                  className="form-check-input"
                  name="priceCourse"
                  type="radio"
                  value="all"
                  id="all"
                />
                همه
              </label>
              <label className="form-check-label">
                <input
                  onClick={radioHndler}
                  className="form-check-input"
                  name="priceCourse"
                  type="radio"
                  value="off"
                  id="off"
                />
             تخفیف ویژه
              </label>
              <label className="form-check-label">
                <input
                  onClick={radioHndler}
                  className="form-check-input"
                  name="priceCourse"
                  type="radio"
                  value="0"
                  id="free"
                />
             رایگان
              </label>
            </div> */}
          </ul>
        </div>
      </div>
    </>
  );
};

export default Filter;
