import React, { useContext, useEffect, useState } from "react";
import ConsultationRequest from "../landing/consultationRequest/ConsultationRequest";
import { Spliter } from "../../tool/spliter/Spliter";
import Steper from "../../tool/steperTool/Steper";
import CourseContent from "./courseContent/CourseContent";
import style from "./BuyCourse.module.css";
import CourseInfo from "./courseInfo/CourseInfo";
import Comments from "../landing/comments/Comments";
import CourseBenefits from "./courseBenefits/CourseBenefits";
import { BASE_URL, getCourseRequest } from "../../apis/urls";
import Loader from "../../tool/loader/Loader";
import { callApi } from "../../apis/callApi";
import { useDispatch, useSelector } from "react-redux";
import {
  getData,
  preloadFalse,
  preloadTrue
} from "../../../redux/buycourse/buyCourseActions";
import Placement from "../../tool/placementText/Placement";
const BuyCourse = () => {
  const state = useSelector((state) => state.stateBuyCourse);

  //const [payment, setPayment] = useState(false);
  const dispatch = useDispatch();
  var id
  useEffect(() => {
    dispatch(preloadTrue());
    const path = window.location.pathname;
    path = path.split("/");
    id = path[path.length - 1];
    const getdatacourse = async () => {
 
    const getdatas = await callApi(
      `${BASE_URL + getCourseRequest}?id=${id}`,
      "{}",
      { "Content-Type": "application/json" },
      "GET"
    );
    if (getdatas[0].code === 200) {
 

      dispatch(getData(getdatas[0].data));

    }
   
 
    };
    getdatacourse();
  }, []);

  return (
    <>

      <div className={`container-fluid mt-5 mb-5`}>
  {
    state.data===null?
    <Placement text="دوره ای یافت نشد"/>:

        <div className="row  flexcenter">
          <div className={`col-lg-10 col-md-11 col-12 ${style.buycourse}`}>
            <div className="row center">
              {state.preload || state.getphonePreload ? (
                <Loader />
              ) : (
                <>
                  <div className={`col-lg-3 col-md-4 col-sm-10 col-12 mb-4 `}>
                    <CourseInfo data={state.data} />
                  </div>
                  <div className="col-lg-9 col-md-8 col-sm-10 col-12">
                    <CourseContent data={state.data} idCourse={id} />
                  </div>
                </>
              )}
            </div>
            <Spliter textSpliter="در خواست مشاوره" />
            <ConsultationRequest />
            <Spliter textSpliter="مزایای این دوره" />
            <CourseBenefits />
            {/* <Spliter textSpliter="دوره های مشابه" />
            <SpecialOff /> */}
            <Spliter textSpliter="نظرات شما" />
            <Comments page="buycourse" />
          </div>

          <Steper activestep={3} />
        </div>
  }
      </div>
     
    </>
  );
};

export default BuyCourse;
