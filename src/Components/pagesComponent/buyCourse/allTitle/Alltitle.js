import React, { useEffect, useState } from "react";
import { callApi } from "../../../apis/callApi";
import { BASE_URL, getCourseTopics } from "../../../apis/urls";
import CourseTitle from "../courseTitle/CourseTitle";
import Loader from "../../../tool/loader/Loader";
import style from "./Alltitle.module.css";
import { useDispatch, useSelector } from "react-redux";
import { getTopics } from "../../../../redux/buycourse/buyCourseActions";
const Alltitle = () => {
  const [preLoad, setPreLoad] = useState(true);
  // const [topics, settopics] = useState();
  const state = useSelector((state) => state.stateBuyCourse);
  const dispatch = useDispatch();
  useEffect(() => {
  
      const path = window.location.pathname;
      path = path.split("/");
      const id = path[path.length - 1];
      const gettitle = async () => {
        const titles = await callApi(
          `${BASE_URL + getCourseTopics}?id=${id}`,
          "{}",
          { "Content-Type": "application/json" },
          "GET"
        );
        if (titles[0].code === 200) {
          dispatch(getTopics(titles[0].data));
          setPreLoad(false);
        }
      };
      gettitle();
  }, []);
  return (
    <>
      <div>
        {state.topicLoading ? (
          <Loader />
        ) : (
          state.topics.map((item) => (
            <>
              <CourseTitle data={item} />
            </>
          ))
        )}
      </div>
    </>
  );
};

export default Alltitle;
