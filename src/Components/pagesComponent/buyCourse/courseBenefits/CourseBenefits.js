import React from "react";
import style from "./CourseBenefits.module.css";
import Benefit from "./benefit/Benefit";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
const CourseBenefits = () => {
  const sliderSettings = {
    dots: true,
    speed: 1500,
    rtl: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    margin: 10,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 2500,
    cssEase: "linear",
    responsive: [
      {
        breakpoint: 1334,
        settings: {
          slidesToShow: 4,
    
        }
      },   {
        breakpoint: 730,
        settings: {
          slidesToShow: 4,
    
        }
      },   {
        breakpoint: 540,
        settings: {
          slidesToShow: 3,
    
        }
      },   {
        breakpoint: 430,
        settings: {
          slidesToShow: 2,
    
        }
      }
    ],
  };
  return (
    <>
      <div className="container-fluid">
        <Slider {...sliderSettings}>
          <Benefit
            title="یادگیری در کمترین زمان "
            img="/assets/images/fast-time.png"
          />
          <Benefit title="کامل بودن دوره " img="/assets/images/checked.png" />
          <Benefit title=" هزینه مناسب" img="/assets/images/dollar.png" />
          <Benefit
            title="مشاوره صفر تا صد "
            img="/assets/images/analysis.png"
          />
          {/* <Benefit
            title="ارائه مدرک"
            img="/assets/images/bachelors-degree.png"
          /> */}
          <Benefit title="تریدر حرفه ای" img="/assets/images/trader.png" />
        </Slider>
      </div>
    </>
  );
};

export default CourseBenefits;
