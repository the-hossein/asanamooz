import React from 'react';
import style from './Benefit.module.css'
const Benefit = ({img , title}) => {
  return <>
<div>
<div className='colStyle'>
   <div className={style.benefit}>
   <img src={img}/>
    <span>{title}</span>
   </div>
</div>

</div>
</>
};

export default Benefit;
