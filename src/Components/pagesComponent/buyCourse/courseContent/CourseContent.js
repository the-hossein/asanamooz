import React, { useEffect, useLayoutEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import MbCourseContent from "../../../../MobileComponents/mbCourseContent/MbCourseContent";
import { getTopics } from "../../../../redux/buycourse/buyCourseActions";
import { callApi } from "../../../apis/callApi";
import { BASE_URL, getCourseTopics } from "../../../apis/urls";
import Loader from "../../../tool/loader/Loader";
import Placement from "../../../tool/placementText/Placement";
import Alltitle from "../allTitle/Alltitle";
import style from "./CourseContent.module.css";
import DateTime from "./dateTimeCourse/DateTime";
import { DefaultPlayer as Video } from 'react-html5video';
import 'react-html5video/dist/styles.css';
const CourseContent = ({ data }) => {

  const [tabDescription, setTabDescription] = useState("intro");
  const [size, setSize] = useState([0]);
  const [preload, setpreload] = useState(false);


  const selectTab = (e) => {
    setTabDescription(e.target.parentElement.getAttribute("name"));
  };

  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth]);
    }
    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);
  const descriptions = () => {
    return { __html: data.description };
  };
  const topics = () => {
    return { __html: data.topics };
  };
  return (
    <>
      <div className="colStyle">
        <div className={style.coursePreveiw}>
       {data.videFile!==null?
            <Video
            id="videocourse"
            // className="video-course"
            // preload="none"
            // poster={data.course_pic}
            // controlsList="nodownload"
            // controls="controls"

              controls={['PlayPause', 'Seek', 'Time', 'Volume', 'Fullscreen']}
              poster={data.course_pic}
          >
            <source src={data.videFile.filepath} type="video/mp4" />
          
            {/* <source src="http://sourcefile.webm" type="video/webm" />
            <track label="English" kind="subtitles" srcLang="en" src="http://source.vtt" default /> */}
          </Video>:
          <img alt="coursePic" src={data.course_pic} className={style.coursePIc}/>
       }
          <div className={style.note}>
            آموختن روش های کسب درآمد از بازار ارزهای دیجیتال یک مهارت است
          </div>
          {size > 500 ? (
            <div className={style.description}>
              <div className={style.tab}>
                <div
                  name="intro"
                  className={tabDescription === "intro" && style.active}
                >
                  <span onClick={selectTab}>معرفی دوره آموزشی </span>
                </div>

                <div
                  name="place"
                  className={tabDescription === "place" && style.active}
                >
                  <span onClick={selectTab}  >مکان و زمان دوره </span>
                </div>

                <div
                  name="title"
                  className={tabDescription === "title" && style.active}
                >
                  <span onClick={selectTab}>سرفصل های دوره </span>
                </div>
              </div>

              <div className={style.content}>
                {tabDescription === "intro" ? (
                  data.description === null ? (
                    <Placement text="هیچ محتوایی نیست" />
                  ) : (
                    <div
                      id={style.des}
                      dangerouslySetInnerHTML={descriptions()}
                    ></div>
                  )
                ) : tabDescription === "place" ? (
                  <>
                    <DateTime data={data} />
                  </>
                ) : // tabDescription === "title" && <Alltitle />
                tabDescription === "title"&&
                
                  <div id={style.topics}dangerouslySetInnerHTML={topics()}></div>
                }
              </div>
            </div>
          ) : (
            <MbCourseContent data={data} />
          )}
        </div>
      </div>
    </>
  );
};

export default CourseContent;
