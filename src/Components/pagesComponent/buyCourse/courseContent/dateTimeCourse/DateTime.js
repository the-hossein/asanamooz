import React from "react";
import { e2p } from "../../../../tool/stringNumberConvert/e2p";
import style from "./DateTime.module.css";
const DateTime = ({ data }) => {
  const start = data.holding_time.split("**$**")[0];
  const end = data.holding_time.split("**$**")[1];
  const day = data.holding_time.split("**$**")[2];

  var persianday;
  switch (day) {
    case "Sunday":
      persianday = "یک شنبه";
      break;
    case "Monday":
      persianday = "دو شنبه";
      break;
    case "Tuesday":
      persianday = "سه شنبه";
      break;
    case "Wednesday":
      persianday = "چهار شنبه";
      break;
    case "Thursday":
      persianday = "پنجشنبه";
      break;
    case "Friday":
      persianday = "جمعه";
      break;
    case "Saturday":
      persianday = "شنبه";
      break;
    default:
      break;
  }

  return (
    <>
      <div className="row center mb-3 mt-3">
        <div className="col-lg-6 mb-3 col-10">
          <div className={style.boxDateTime}>
            <div>
              <img src="/assets/images/placeholder.png" />
              <span>مکان برگزاری کلاس</span>
            </div>
            <div className={style.address}>{data.location}</div>
          </div>
        </div>
        <div className="col-lg-6 col-10">
          <div className={style.boxDateTime}>
            <div>
              <img src="/assets/images/time.png" />
              <span>زمان برگزاری کلاس</span>
            </div>
            <div className={style.date}>
              {data.holding_time === "Private" ? (
               <p className={style.private}> با هماهنگی شما</p>
              ) : (
                <>
                  <span>{persianday} ها </span>
                  <span>
                    {e2p(start)} - {e2p(end)}
                  </span>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DateTime;
