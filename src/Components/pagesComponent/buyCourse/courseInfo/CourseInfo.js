import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faInfoCircle,
  faUser,
  faMapMarkerAlt,
  faCalendarAlt,
  faClock,
  faEdit,
  faCalendarCheck,
  faClipboardList,
  faCalendarWeek,
  faUsers
} from "@fortawesome/free-solid-svg-icons";
import React, { useContext, useEffect, useLayoutEffect, useState } from "react";
import style from "./CourseInfo.module.css";
import SecondlyButton from "../../../tool/secondlyButton/SecondlyButton";
import convertDate from "../../../tool/convertDate/convertDate";
import persianNumber from "../../../tool/persianNumber/persianNumber";
import { callApi } from "../../../apis/callApi";
import {
  addBasket,
  BASE_URL,
  checkUserClass,
  getProfile,
  packageDetails
} from "../../../apis/urls";
import Getphone from "../../../getPhoneUser/Getphone";
import TabBar2 from "../../../../MobileComponents/tabBar2/TabBar2";

import { useRouter } from "next/router";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../../../tool/loader/Loader";
import { userNotLogined } from "../../../../redux/login/loginActions";
import {
  statusBuyCourse,
  statusLoadingFalse,
  statusLoadingTrue
} from "../../../../redux/buycourse/buyCourseActions";
import {
  showPopup,
  statusGetPhone
} from "../../../../redux/getphone/getphoneAction";
import daysLeft from "../../../tool/daysLeft/daysLeft";
import { e2p } from "../../../tool/stringNumberConvert/e2p";
import { notify } from "../../../tool/toast/toast";

const CourseInfo = ({ data }) => {
  const router = useRouter();
  const [popup, setPopUp] = useState(false);
  const [userID, setUserID] = useState();
  const [size, setSize] = useState([0]);
  const [totalPrice, setTotalPrice] = useState(data.price);
  const [preLoad, setpreLoad] = useState(false);
  const [typeOfCourse, setTypeOfCourse] = useState();
  const [off, setOff] = useState(0);
  const [courseStatus, setCourseStatus] = useState("");
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  if (typeof window !== "undefined") {
    var phoneLS = localStorage.getItem("userToken");
    const path = window.location.pathname;
    path = path.split("/");
    var classid = path[path.length - 1];
  }

  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth]);
    }
    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);
  const start = data.holding_time.split("**$**")[0];
  const end = data.holding_time.split("**$**")[1];
  const day = data.holding_time.split("**$**")[2];

  var persianday;
  switch (day) {
    case "Sunday":
      persianday = "یک شنبه";
      break;
    case "Monday":
      persianday = "دو شنبه";
      break;
    case "Tuesday":
      persianday = "سه شنبه";
      break;
    case "Wednesday":
      persianday = "چهار شنبه";
      break;
    case "Thursday":
      persianday = "پنجشنبه";
      break;
    case "Friday":
      persianday = "جمعه";
      break;
    case "Saturday":
      persianday = "شنبه";
      break;
    default:
      break;
  }
  useEffect(() => {
    // setpreLoad(true);

    dispatch(statusLoadingTrue());
    if (data.isEnable === false) {
      dispatch(statusBuyCourse("disable"));
    }
    if (state.stateLogin.loginStatus) {
      var phone = JSON.parse(phoneLS).phone;
      var token = JSON.parse(phoneLS).token;
      var myHeaders = new Headers();
      myHeaders.append("Authorization", `Bearer ${token}`);
      myHeaders.append("Content-Type", "application/json");

      let checkClass = async () => {
        let classStatus = await callApi(
          `${BASE_URL + checkUserClass}?UserId=${
            state.stateLogin.userData.id
          }&ClassId=${classid}`,
          "{}",
          myHeaders,
          "GET"
        );

        if (classStatus[0].data === true) {
          dispatch(statusBuyCourse("bought"));
        } else {
          dispatch(statusBuyCourse());
        }
      };
      checkClass();
    } else {
      dispatch(userNotLogined());
      dispatch(statusBuyCourse());

      // setpreLoad(false);
    }
    if (data.offCode !== null) {
      if (daysLeft(data.offCode.enddate) > 0) {
        if (data.offCode.amount !== null) {
          setTotalPrice(data.price - data.offCode.amount);
          setOff(data.offCode.amount);
          if (data.price - data.offCode.amount <= 0) {
            setTotalPrice(0);
          }
        }
        if (data.offCode.percent !== null) {
          setTotalPrice(((100 - data.offCode.percent) * data.price) / 100);
          setOff(data.offCode.amount);
          if (((100 - data.offCode.percent) * data.price) / 100 <= 0) {
            setTotalPrice(0);
          }
        }
      } else {
        setTotalPrice(data.price);
        setOff(0);
      }
    }
    if (data.price === 0) {
      setTotalPrice(0);
    }
    if (data.type_course === "Offline") {
      setTypeOfCourse("حضوری");
    } else {
      setTypeOfCourse("غیرحضوری");
    }
  }, []);

  const buyHndler = () => {
    dispatch(statusGetPhone("buy"));
    // setpreLoad(true);
    dispatch(statusLoadingTrue());

    if (state.stateLogin.loginStatus === true) {
      if (phoneLS) {
        var phone = JSON.parse(phoneLS).phone;
        var token = JSON.parse(phoneLS).token;
      }
      var myHeaders = new Headers();
      myHeaders.append("Authorization", `Bearer ${token}`);
      myHeaders.append("Content-Type", "application/json");
      const path = window.location.pathname;
      path = path.split("/");
      const ClassID = path[path.length - 1];
      if (state.stateBuyCourse.status !== "bought") {
        const PackageDeatil = async () => {
          const deatils = await callApi(
            `${BASE_URL + packageDetails}?id=${ClassID}`,
            "{}",
            { "Content-Type": "application/json" },
            "GET"
          );
          if (deatils[0].code === 200) {
            const AddBasket = async () => {
              const raw = JSON.stringify({
                userid: state.stateLogin.userData.id,
                basket_title: `${data.title}خرید دوره`,
                phone_number: phone,
                description: `${data.title}خرید دوره`,
                qty: 1,
                amount: totalPrice,
                package_detail: deatils[0].data
              });

              const paymentData = await callApi(
                BASE_URL + addBasket,

                raw,
                myHeaders,
                "POST"
              );

              if (paymentData[0].code === 200 && paymentData[0].data !== null) {
                if (state.stateBuyCourse.status === "bought") {
                  dispatch(statusBuyCourse("bought"));
                } else {
                  router.push({
                    pathname: "/payment"
                  });
                }
              }
            };
            AddBasket();
          }
        };
        PackageDeatil();
      }
    } else {
      dispatch(showPopup());
      dispatch(statusLoadingFalse());
    }
  };
  const disableCourse = () => {
    notify("امکان دسترسی به این دوره وجود ندارد", "warning");
  };
  return (
    <div>
      {state.stateGetPhone.show && (
        <Getphone
          setPopUp={setPopUp}
          content="شماره تماس خود را وارد کنید"
          button="ثبت"
          status="buyCourse"
          totalPrice={totalPrice}
          pages="buyCourse"
        />
      )}
      <div className={`colStyle ${style.inforamtion}`}>
        <div className={style.title}>
          <p>اطلاعات دوره</p>
        </div>
        <div className={style.detailsInfo}>
          <ul>
            <li>
              <FontAwesomeIcon icon={faInfoCircle} />
              <span>{data.title}</span>
            </li>
            <li>
              <FontAwesomeIcon icon={faUser} />
              مدرس:
              <span>
                {data.users.name} {data.users.family}
              </span>
            </li>
            <li>
              <FontAwesomeIcon icon={faClipboardList} />
              پیش نیاز دوره:
              <span>
                {data.course_level === "پیشرفته"
                  ? "آشنایی با ارز های دیجیتال (مقدماتی)"
                  : "این دوره پیش نیازی ندارد"}
              </span>
            </li>
            <li>
              <FontAwesomeIcon icon={faMapMarkerAlt} />
              نوع دوره:
              <span>{typeOfCourse}</span>
            </li>

            <li>
              <FontAwesomeIcon icon={faClock} />
              مدت زمان دوره:
              <span>{persianNumber(data.duration)} ساعت </span>
            </li>
            <li>
              <FontAwesomeIcon icon={faCalendarAlt} />
              تعدادجلسات:
              <span> {persianNumber(data.session_count)} جلسه</span>
            </li>
            <li>
              <FontAwesomeIcon icon={faCalendarWeek} />
              زمان دوره:
              <span>
                {data.holding_time === "Private"
                  ? "با هماهنگی شما"
                  : persianday + " ها " + e2p(start) + " تا " + e2p(end)}
              </span>
            </li>
            {data.holding_time === "Private" ? (
              <li>
                <FontAwesomeIcon icon={faUsers} />
                ظرفیت دوره :<span> {" تا " + data.capcity + " نفر "}</span>
              </li>
            ) : (
              <li>
                <FontAwesomeIcon icon={faCalendarCheck} />
                تاریخ شروع:<span> {convertDate(data.start_date)}</span>
              </li>
            )}
            <li>
              <FontAwesomeIcon icon={faEdit} />
              تاریخ آخرین بروزرسانی:
              <span>{convertDate(data.last_update)} </span>
            </li>
          </ul>
        </div>
        <div className={style.buy}>
          <div className={style.price}>
            {off !== 0 ? (
              <>
                <span className={style.off}>
                  {persianNumber(data.price)} تومان
                </span>
                <span className={style.totalPrice}>
                  {persianNumber(totalPrice)} تومان
                </span>
              </>
            ) : (
              <span className={style.totalPrice}>
                {persianNumber(totalPrice)} تومان
              </span>
            )}
          </div>
          {state.stateBuyCourse.status === "bought" ? (
            <Link href={`/classprograms?CourseId=${classid}`}>
              <div>
                <SecondlyButton
                  text={"شما این دوره را تهیه کرده اید"}
                  preLoad={state.stateBuyCourse.statusLoading}
                />
              </div>
            </Link>
          ) : data.isEnable === false ? (
            <div onClick={disableCourse}>
              <SecondlyButton
                text={"این دوره غیرفعال می باشد"}
                preLoad={state.stateBuyCourse.statusLoading}
              />
            </div>
          ) : (
            <div onClick={buyHndler}>
              <SecondlyButton
                text={"خرید دوره"}
                preLoad={state.stateBuyCourse.statusLoading}
              />
            </div>
          )}
        </div>
      </div>

      {size < 500 ? (
        state.stateBuyCourse.status === "bought" ? (
          <Link href={`/classprograms?CourseId=${classid}`}>
            <div>
              <TabBar2
                text={"شما این دوره را تهیه کرده اید"}
                preLoad={state.stateBuyCourse.statusLoading}
                coursePrice={totalPrice}
                title="قیمت این دوره"
              />
            </div>
          </Link>
        ) : data.isEnable === false ? (
          <div onClick={disableCourse}>
            <TabBar2
              text={"این دوره غیرفعال می باشد"}
              title="قیمت این دوره"
              coursePrice={totalPrice}
              preLoad={state.stateBuyCourse.statusLoading}
            />
          </div>
        ) : (
          <TabBar2
            text={"خرید دوره"}
            preLoad={state.stateBuyCourse.statusLoading}
            coursePrice={totalPrice}
            title="قیمت این دوره"
            buyHndler={buyHndler}
          />
        )
      ) : (
        ""
      )}
    </div>
  );
};

export default CourseInfo;
