import React from "react";
import style from "./CourseTitle.module.css";
const CourseTitle = (data) => {
  return (
    <>
      <div className={style.heading}>
        <div>
          <div className={style.titleStyle}>
            <span>{data.data.topicNumber}</span>
            <span>{data.data.title}</span>
          </div>
          {/* <button className={style.downloadbtn}>دانلود</button> */}
        </div>
      </div>
    </>
  );
};

export default CourseTitle;
