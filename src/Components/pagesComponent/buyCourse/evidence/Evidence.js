import React from "react";
import style from "./Evidence.module.css";
const Evidence = () => {
  return (
    <>
      <div className={`colStyle ${style.evidence}`}>
        <div className={`row ${style.reverse}`}>
          <div className={`col-lg-6 col-md-6 ${style.content} `}>
            <div>
              <p>مدرک بین المللی</p>
              <p>
                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله
                در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد
                نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان
                جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را
                برای طراحان رایانه ای علی
              </p>
            </div>
          </div>
          <div className={`col-lg-6 col-md-6`}>
            <div className={style.evidenceImage}>

            <img src="/assets/images/evidence.jpg" alt="evidence image" />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Evidence;
