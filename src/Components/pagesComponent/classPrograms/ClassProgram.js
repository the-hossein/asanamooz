import React from "react";
import TabBar from "../../../MobileComponents/tabBar/TabBar";
import Steper from "../../tool/steperTool/Steper";
import Classes from "./classes/Classes";
import style from "./ClassProgram.module.css";
const ClassProgram = () => {
  return (
    <>
      <div className="container-fluid mt-5 mb-5">
        <div className="row flexcenter">
          <div className={`col-lg-10 col-md-11 col-12 `}>
            <Classes />
          </div>

          <Steper activestep={5} />
        </div>
      </div>
    </>
  );
};

export default ClassProgram;
