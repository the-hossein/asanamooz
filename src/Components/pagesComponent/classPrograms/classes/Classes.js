import React, { useEffect, useState } from "react";
import { useLayoutEffect } from "react";
import MobileClassProgram from "../../../../MobileComponents/mobileClassProgram/MobileClassProgram";
import { callApi } from "../../../apis/callApi";
import {
  BASE_URL,
  checkUserClass,
  getBasket,
  getCourseRequest,
  getMeeting,
  getProfile
} from "../../../apis/urls";
import Loader from "../../../tool/loader/Loader";
import Placement from "../../../tool/placementText/Placement";
import style from "./Classes.module.css";
import ClassRow from "./classRow/ClassRow";

const Classes = () => {
  const [record, setRecord] = useState();
  const [preLoad, setpreLoad] = useState(true);
  const [course, setCourse] = useState();
  const [size, setSize] = useState([0]);
  const [classStatus, setClassStatus] = useState();
  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth]);
    }
    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);
  if (typeof window !== "undefined") {
    var ls = localStorage.getItem("userToken");
  }

  useEffect(() => {
    if (ls) {
      var phone = JSON.parse(ls).phone;
      var token = JSON.parse(ls).token;
      var myHeaders = new Headers();
      myHeaders.append("Authorization", `Bearer ${token}`);
      myHeaders.append("Content-Type", "application/json");
      const search = window.location.search;
      const params = new URLSearchParams(search);
      const classid = params.get("CourseId");

      var raw = JSON.stringify({
        methodname: 3,
        loginWithPassword: {
          userName: phone
        }
      });
      let userProfile = async () => {
        const profile = await callApi(
          BASE_URL + getProfile,
          raw,
          myHeaders,
          "POST"
        );
        if (profile[0].code === 200) {
          let userid = profile[0].data.id;
          let checkClass = async () => {
            let classStatus = await callApi(
              `${
                BASE_URL + checkUserClass
              }?UserId=${userid}&ClassId=${classid}`,
              "{}",
              myHeaders,
              "GET"
            );

            if (classStatus[0].data === true) {
              setClassStatus(true);

              const getprograms = async () => {
                const program = await callApi(
                  `${BASE_URL + getMeeting}?CourseId=${classid}`,
                  "{}",
                  { "Content-Type": "application/json" },
                  "GET"
                );

                setRecord(program[0].data);
                const GetCourse = async () => {
                  const getcourse = await callApi(
                    `${BASE_URL + getCourseRequest}?id=${classid}`,
                    "{}",
                    { "Content-Type": "application/json" },
                    "GET"
                  );
                  setCourse(getcourse[0].data);
                  setpreLoad(false);
                };

                GetCourse();
              };
              getprograms();
            } else {
              setpreLoad(false);
              setClassStatus(false);
            }
          };
          checkClass();
        }
      };
      userProfile();
    }
  }, []);
  return (
    <>
      {size < 500 ? (
        preLoad ? (
          <Loader />
        ) : classStatus ? (
          <MobileClassProgram
            course={course}
            record={record}
            preLoad={preLoad}
            setpreLoad={setpreLoad}
          />
        ) : (
          <Placement text="در این دوره ثبت نام نشده اید" />
        )
      ) : (
        <div className="colStyle">
          <div className={style.program}>
            <p className={style.title}>
              برنامه کلاسی دوره های آموزشی آسان بیت کوین
            </p>
            {classStatus ? (
              <>
                <div className={style.programHeader}>
                  {preLoad ? (
                    <Loader />
                  ) : (
                    <>
                      <div>
                        <p>{course.title}</p>
                        <p>
                          <img src="assets/images/teacher.png" />
                          {course.users.name} {course.users.family}
                        </p>
                      </div>
                      <div>
                        <p> تعداد جلسات دوره: {course.session_count}</p>
                        <p>آدرس کلاس: {course.location}</p>
                      </div>
                    </>
                  )}
                </div>
                <div className={style.tableprogram}>
                  <table className={`table table-borderless`}>
                    <thead>
                      <tr>
                        <th>ردیف </th>
                        <th>جلسات </th>
                        <th>توضیحات </th>
                        <th>تاریخ کلاس </th>
                        <th>ساعت کلاس </th>
                        <th>وضعیت کلاس </th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                     preLoad===false&&
                        record.map((item) => (
                          <>
                            <ClassRow data={item} />
                          </>
                        ))
                      }
                    </tbody>
                  </table>
            {  preLoad===false&&
              record.length == 0 &&
                <Placement text="برنامه ای نیست" />
            }
                </div>
                <div className={`${style.evidence} ${style.disable}`}>
                  <span>پرینت برنامه کلاسی</span>
                  <button className="mainBtnStyle"> پرینت برنامه</button>
                </div>
              </>
            ) : classStatus === false ? (
              <Placement text="در این دوره ثبت نام نشده اید" />
            ) : (
              <Loader />
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default Classes;
