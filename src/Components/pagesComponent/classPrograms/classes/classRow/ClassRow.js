import React from "react";
import persianNumber from "../../../../tool/persianNumber/persianNumber";
import Placement from "../../../../tool/placementText/Placement";
import { e2p } from "../../../../tool/stringNumberConvert/e2p";
import style from "./ClassRow.module.css";
const ClassRow = ({ data }) => {
  const status = data.stateType.title;
console.log(data.holdingTime)

  return (
    <>
      {

        <tr>
          <td>{data.orderNo}</td>
          <td>{data.title}</td>
          <td>{data.description}</td>
          <td>
        
            {new Date(data.startDatetime.split("T")[0]).toLocaleDateString(
              "fa-IR"
            )}
          </td>
          <td>{e2p(data.holdingTime)}</td>
          <td
            className={
              status === "اتمام کلاس"
                ? style.finished
                : status === "برگذار نشده"
                ? style.onperforming
                : style.notheld
            }
          >
            {status}
          </td>
        </tr>
      }
    </>
  );
};

export default ClassRow;
