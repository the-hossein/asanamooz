import React from "react";
import style from "./Landing.module.css";
import { Spliter } from "../../tool/spliter/Spliter";
import Comments from "./comments/Comments";
import ConsultationRequest from "./consultationRequest/ConsultationRequest";
import Intro from "./intro/Intro";

import Steper from "../../tool/steperTool/Steper";

const Landing = () => {
  return (
    <div className={`container-fluid mt-5 mb-5 ${style.landing}`}>
      <div className="row flexcenter">
        <div className={`col-lg-10 col-md-11 col-12 `}>
          <Intro />
          <Spliter textSpliter="درخواست مشاوره" />
          <ConsultationRequest />
          {/* <Spliter textSpliter="تخفیف های ویژه" />
      <SpecialOff /> */}
          <Spliter textSpliter="نظرات شما" />
          <Comments page="landing" />
        </div>

        <Steper activestep={1} />
      </div>
    </div>
  );
};

export default Landing;
