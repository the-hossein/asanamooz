import React, { useEffect, useState } from "react";
import { callApi } from "../../../apis/callApi";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  addComments,
  BASE_URL,
  getallComments,
  getpackagecomment,
  getProfile
} from "../../../apis/urls";
import Loader from "../../../tool/loader/Loader";
import MainButton from "../../../tool/mainButton/MainButton";
import style from "./Comments.module.css";
import UserComment from "./userComment/UserComment";
import { validateCm } from "./validateCm";
import { useSelector } from "react-redux";
import { notify } from "../../../tool/toast/toast";
const Comments = ({ page }) => {
  const state = useSelector((state) => state.stateLogin);
  const [replayShow, setReplyShow] = useState(true);
  const [preLoad, setpreload] = useState(true);
  const [preloadCm, setPreloadCm] = useState(false);
  const [commentData, setCommentData] = useState();
  const [allComment, setAllComments] = useState();
  const [showCommnet, setShowComment] = useState();
  const [userID, setUserID] = useState("");
  const [submitCm, setSubmitCm] = useState();
  const [info, setInfo] = useState({
    fullname: "",
    number: "",
    email: "",
    comment: ""
  });
  const [errors, setError] = useState({});
  const [touched, setTouched] = useState({});
  var
  persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
  arabicNumbers  = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g],
  fixNumbers = function (str)
  {
    if(typeof str === 'string')
    {
      for(var i=0; i<10; i++)
      {
        str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
      }
    }
    return str;
  };
  if (typeof window !== "undefined") {
    var ls = localStorage.getItem("userToken");
    var path = window.location.pathname;
    path = path.split("/");
    var id = path[path.length - 1];
  }
  useEffect(() => {
    setError(validateCm(info));
  }, [info, touched]);
  useEffect(() => {
    if (ls) {
      var phone = JSON.parse(ls).phone;
setInfo({...state,number:phone})


    }    //   var token = JSON.parse(ls).token;
    //   setInfo({ ...info, number: phone });

    //   let userProfile = async () => {
    //     var myHeaders = new Headers();
    //     myHeaders.append("Authorization", `Bearer ${token}`);
    //     myHeaders.append("Content-Type", "application/json");
    //     var raw = JSON.stringify({
    //       methodname: 3,
    //       loginWithPassword: {
    //         userName: phone
    //       }
    //     });
    //     const profile = await callApi(
    //       BASE_URL + getProfile,
    //       raw,
    //       myHeaders,
    //       "POST"
    //     );
    //     console.log(profile[0]);
    //     if (profile[0].code === 200 && profile[0].data !== null) {
    //       setUserID(profile[0].data.id);
    //     }
    //   };
    //   userProfile();
    // }
    if (page === "landing") {
      const allComent = async () => {
        const allCm = await callApi(
          BASE_URL + getallComments,
          "{}",
          { "Content-Type": "application/json" },
          "GET"
        );

        setAllComments(allCm[0].data);
        setCommentData(
          allCm[0].data
            .filter((item) => {
              setReplyShow(false);
              return item.packageCommentId === null;
            })
            .reverse()
        );
        setShowComment(
          allCm[0].data
            .filter((item) => {
              setReplyShow(false);
              return item.packageCommentId === null;
            })
            .slice(0, 4)
        );

        setpreload(false);
      };
      allComent();
    } else {
      const pckageComment = async () => {
        const comments = await callApi(
          `${BASE_URL + getpackagecomment}?ClassId=${id}`,
          "{}",
          { "Content-Type": "application/json" },
          "GET"
        );
        if (comments[0].code === 200) {
          setAllComments(comments[0].data);
          setCommentData(
            comments[0].data
              .filter((item) => {
                setReplyShow(false);
                return item.packageCommentId === null;
              })
              .reverse()
          );
          setShowComment(
            comments[0].data
              .filter((item) => {
                setReplyShow(false);
                return item.packageCommentId === null;
              })
              .slice(0, 4)
          );
        }
        setpreload(false);
      };
      pckageComment();
    }
  }, []);
  const changeHandler = (e) => {
    if(e.target.id!=="number"){

      setInfo({ ...info, [e.target.id]: e.target.value });
    }
  };
  const changeHandlerNumber=(e)=>{
    
    setInfo({...info,number:fixNumbers(e.target.value)})
    console.log(info.number)
  }
  const focusHandler = (e) => {
    setTouched({ ...touched, [e.target.id]: true });
  };

  const submitComment = () => {
    setSubmitCm(false);
    setPreloadCm(true);
    if (!Object.keys(errors).length) {
      var raw;
      if (state.loginStatus && page === "landing") {
        raw = JSON.stringify({
          fullname: info.fullname,
          email: info.email,
          text: info.comment,
          phonenumber: info.number,
          // "classid": id,
          userid: userID
        });
      }
      if (state.loginStatus && page !== "landing") {
        raw = JSON.stringify({
          fullname: info.fullname,
          email: info.email,
          text: info.comment,
          phonenumber: info.number,
          classid: id,
          userid: userID
        });
      }
      if (state.loginStatus === false && page === "landing") {
        raw = JSON.stringify({
          fullname: info.fullname,
          email: info.email,
          text: info.comment,
          phonenumber: info.number
          // "classid": id,
          // "userid": userID
        });
      }
      if (state.loginStatus === false && page !== "landing") {
        raw = JSON.stringify({
          fullname: info.fullname,
          email: info.email,
          text: info.comment,
          phonenumber: info.number,
          classid: id
          // "userid": userID
        });
      }
      console.log(raw);
      const addCm = async () => {
        const adding = await callApi(
          BASE_URL + addComments,
          raw,
          { "Content-Type": "application/json" },
          "POST"
        );
        console.log(adding[0]);
        if (adding[0].code === 200) {
          setpreload(false);
          setPreloadCm(false);
          notify("نظر شما در صف بررسی قرار گرفت", "success");
          setSubmitCm(true);

          setInfo({
            fullname: "",
            number: "",
            email: "",
            comment: ""
          });
          setTouched({
            fullname: false,
            number: false,
            email: false,
            comment: false
          });
          setTimeout(() => {
            setSubmitCm(false);
          }, 1000);
        } else {
          setPreloadCm(false);
        }
      };
      addCm();
    } else {
      setPreloadCm(false);

      setTouched({
        fullname: true,
        number: true,
        email: true,
        comment: true
      });
    }
  };
  const more = () => {
    setShowComment(commentData);
    // setShowComment({
    //   num: showCommnet.num + 3,
    //   data: commentData.slice(0, showCommnet.num)
    // });
    // if (showCommnet.num >= commentData.length) {
    //   console.log("first");
    // }
  };
  return (
    <>
      <div className={`col-lg-10 colStyle ${style.comments}`}>
        <p>نظرات خود را با ما در میان بگذارید</p>

        <div>
          <div className={style.userdata}>
            <div>
              <input
                value={info.fullname}
                onChange={changeHandler}
                onFocus={focusHandler}
                id="fullname"
                type="text"
                className="form-control"
                placeholder="نام و نام خانوادگی"
              />
              {errors.fullname && touched.fullname && (
                <span className={style.invalid}>{errors.fullname}</span>
              )}
            </div>
            <div>
              <input
                value={info.number}
                onChange={changeHandlerNumber}
                onFocus={focusHandler}
                id="number"
                type="text"
                className="form-control"
                placeholder="شماره همراه"
                maxLength={11}
              />
              {errors.number && touched.number && (
                <span className={style.invalid}>{errors.number}</span>
              )}
            </div>

            <div>
              <input
                value={info.email}
                onChange={changeHandler}
                onFocus={focusHandler}
                id="email"
                type="email"
                className="form-control"
                placeholder="ایمیل"
              />
              {errors.email && touched.email && (
                <span className={style.invalid}>{errors.email}</span>
              )}
            </div>
          </div>

          <div className={style.commnetText}>
            <textarea
              value={info.comment}
              onChange={changeHandler}
              onFocus={focusHandler}
              id="comment"
              className="form-control"
              placeholder="متن نظر"
            ></textarea>
            {errors.comment && touched.comment && (
              <span className={style.invalid}>{errors.comment}</span>
            )}
          </div>
          {preloadCm ? <Loader /> : ""}

          <div className={style.submitComment}>
            <div onClick={submitComment}>
              <MainButton text="ثبت نظر" />
            </div>
          </div>
        </div>
      </div>
      <div className={`colStyle col-lg-10  mt-3 ${style.AllComment}`}>
        {preLoad ? (
          <Loader />
        ) : (
          replayShow === false &&
          showCommnet.map((item) => (
            <>
         
              <UserComment key={item.id} data={item} alldata={allComment} />
            </>
          ))
        )}
      </div>

      {preLoad === false && showCommnet.length !== commentData.length && (
        <button className={style.more} onClick={more}>
          نظرات بیشتر
          <FontAwesomeIcon className={style.arrowndown} icon={faChevronDown} />
        </button>
      )}
    </>
  );
};

export default Comments;
