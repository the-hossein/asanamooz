import React, { useEffect, useState } from "react";
import { callApi } from "../../../../apis/callApi";
import { BASE_URL, getallComments } from "../../../../apis/urls";
import convertDate from "../../../../tool/convertDate/convertDate";
import style from "./UserComment.module.css";
const UserComment = ({ data, alldata }) => {
  const [reply, setReply] = useState(null);
  const [preload, setPreload] = useState(true);
  var test;
  useEffect(() => {
    alldata.find((item) => {
      if (item.packageCommentId === data.id) {
        setPreload(false);
        return setReply(item);
      }
    });
  }, []);
  return (
    <>
      <div className={` mt-2 mb-2 ${style.commentstyles}`}>
        <div className={style.userComment}>
          <div className="flex">
            <img src="/assets/images/user.png" />

            <div className={style.deatails}>
              <span>{data.fullname}</span>
              <span>{convertDate(data.createdDatetime)}</span>
            </div>
          </div>
          <p>{data.commentText}</p>
        </div>
        {reply !== null && preload == false && (
          <div className={style.admin}>
            <div>
              <img src="/assets/images/reply.png" />
              <img src="/assets/images/admin.png" />
              پاسخ
            </div>
            <p>{reply.commentText}</p>
          </div>
        )}
      </div>
    </>
  );
};

export default UserComment;
