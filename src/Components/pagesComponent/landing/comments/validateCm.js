export const validateCm = (info) => {
  const errors = {};

  if (!info.fullname) {
    errors.fullname = "لطفا نام  خود را وارد کنید";
  } else delete errors.fullname;

  if (!info.number) {
    errors.number = "لطفا شماره همراه خود را وارد کنید";
  } else if (info.number.length < 11) {
    errors.number = "شماره همراه خود را به درستی وارد کنید";
  } else if (
    !/(\+98|0)?9\d{9}/.test(info.number) &&
    info.number.length === 11
  ) {
    errors.number = "شماره همراه شما صحیح نیست";
  } else delete errors.number;

  if (!info.email) {
    errors.email = "لطفا ایمیل خود را وارد کنید";
  } else if (!/\S+@\S+\.\S+/.test(info.email)) {
    errors.email = "ایمیل نامعتبر  می باشد";
  } else delete errors.email;

  if (!info.comment) {
    errors.comment = "وارد کردن نظر الزامی می باشد";
  } else delete errors.comment;

  return errors;
};
