import React, { useState } from "react";
import Getphone from "../../../getPhoneUser/Getphone";
import MainButton from "../../../tool/mainButton/MainButton";
import style from "./ConsultationRequest.module.css";
import supportSvg from "../../../../../public/assets/images/support.svg";
import Image from "next/image";
import { useDispatch, useSelector } from "react-redux";
import { showPopup, statusGetPhone } from "../../../../redux/getphone/getphoneAction";
const ConsultationRequest = () => {
  // const [popup, setPopUp] = useState(false);
  const state=useSelector(state=>state.stateGetPhone)
  const dispatch=useDispatch()
  const requestHndler = () => {
    dispatch(statusGetPhone("support"))

  dispatch(showPopup())
  };
  return (
    <>
      {state.show && <Getphone pages="support" />}
      <div className={`col-10 colStyle   ${style.request}`}>
        <Image src={supportSvg} alt="supportImage" height={260} width={300} />
        {/* <img src="/assets/images/support.svg" /> */}
        <div>
          <span className={style.content}>
            برای کسب اطلاعات بیشتر با ما تماس بگیرید
          </span>
          <div onClick={requestHndler}>
            <MainButton text=" درخواست مشاوره" />
          </div>
        </div>
      </div>
    </>
  );
};

export default ConsultationRequest;
