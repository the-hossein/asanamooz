import Image from "next/image";
import Link from "next/link";
import React, { useState } from "react";
import style from "./Intro.module.css";
import intro from "../../../../../public/assets/images/intro.svg";
const Intro = () => {
  const [preLoad, setpreLoad] = useState(false);

  return (
    <>
      <div className={`col-lg-12 col-12 colStyle ${style.intro} `}>
        
        <div className="row center alignCenter  ">
          <div className={`col-xl-6 col-lg-6  col-md-6 col-12 `}>
            <div className={style.introText}>
              <div className={style.title}>
                <Image src={intro} alt="introImage" />

                <h4>آسان آموز چیست؟</h4>
              </div>
              <p>
              آسان آموز به شما کمک می کند با دنیای ارزهای دیجیتال به شکلی اصولی آشنا شده و مهارت های لازم برای معاملات موفق در این حوزه را بیاموزید و سودآوری را عملا تجربه کنید.

<h3 className={style.boldText}>راز موفقیت دانستن است</h3>
              </p>
              <div className={style.register}>
                <Link href="/asanamooz">
                  <div
                    className={style.registerBtn}
                    onClick={() => setpreLoad(true)}
                  >
                    <div className={style.btnText}>
                      <span>ثبت نام دوره ها</span>
                      <span>
                        {" "}
                        {preLoad ? (
                          <div
                            className="spinner-border text-warning"
                            role="status"
                          >
                            <span className="sr-only"></span>
                          </div>
                        ) : (
                          " با آسان آموز، آسان بیاموز"
                        )}
                      </span>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
          <div className={`col-xl-6 col-lg-6 col-md-6  ${style.introimg}`}>
            <Image src={intro} alt="introImage" />
          </div>
        </div>
      </div>
    </>
  );
};

export default Intro;
