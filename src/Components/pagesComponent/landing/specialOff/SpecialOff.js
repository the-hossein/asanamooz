import React, { useEffect, useState } from "react";
import style from "./SpecialOff.module.css";
import Course from "../../../course/Course";
import Slider from "react-slick";
import Loader from '../../../tool/loader/Loader'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { callApi } from "../../../apis/callApi";
import { BASE_URL, getAllCoursesRequest } from "../../../apis/urls";
const SpecialOff = () => {
  const [preload, setPreoad] = useState(true);
  const [course, setCourse] = useState();
  useEffect(() => {
    const getcourse = async () => {
      const getdatas = await callApi( BASE_URL+getAllCoursesRequest,"{}",{'Content-Type': 'application/json'},"GET");
      const datas=getdatas[0].data.result
      setCourse(datas);
      setPreoad(false);
    };
    return getcourse();
  }, []);

  const sliderSettings = {
    dots: true,
    speed: 2000,
    slidesToShow: 4,
    slidesToScroll: 4,
    margin: 10,
    arrows: true,
    responsive: [
      {
        breakpoint: 1110,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 527,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <>
      <div className={`container-fluid ${style.allCourse}`}>
        <Slider {...sliderSettings}>
          {preload ? (
            <Loader/>
          ) : (
            course.map((item) => (
              <>
                <Course data={item} key={item.classID} />
              </>
            ))
          )}
        </Slider>
      </div>
    </>
  );
};

export default SpecialOff;
