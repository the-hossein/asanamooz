import React, { useContext, useEffect, useLayoutEffect, useState } from "react";
import Steper from "../../tool/steperTool/Steper";
import ChosePort from "./chosePort/ChosePort";
import Cart from "./cart/Cart";
import style from "./Payment.module.css";
import TabBar2 from "../../../MobileComponents/tabBar2/TabBar2";

import { useRouter } from "next/router";
import { BASE_URL, getBasket, getProfile } from "../../apis/urls";
import { callApi } from "../../apis/callApi";
import Loader from "../../tool/loader/Loader";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingBasket } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import MainButton from "../../tool/mainButton/MainButton";
import { useDispatch, useSelector } from "react-redux";
import {
  basketpreloadTure,
  getbasketData
} from "../../../redux/payment/paymentActios";
const Payment = () => {
  const router = useRouter();
  const state = useSelector((state) => state.statePayment);
  const dispatch = useDispatch();
  const [preLoad, setPreLoad] = useState(true);
  const [emptyBasket, setEmptyBasket] = useState();
  const [data, setData] = useState();
  if (typeof window !== "undefined") {
    var ls = localStorage.getItem("userToken");
  }
  useEffect(() => {
    dispatch(basketpreloadTure());
    if (ls) {
      var number = JSON.parse(ls).phone;
      var token = JSON.parse(ls).token;

      var myHeaders = new Headers();
      myHeaders.append("Authorization", `Bearer ${token}`);
      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify({
        methodname: 3,
        loginWithPassword: {
          userName: number
        }
      });
      let userProfile = async () => {
        try {
          const profile = await callApi(
            BASE_URL + getProfile,
            raw,
            myHeaders,
            "POST"
          );
          if (profile[0].code === 200 && profile[0].data !== null) {
            var userId = profile[0].data.id;
          }
        } catch {
          
          setEmptyBasket(true);
        }

        const GetBasket = async () => {
          try {
            const basket = await callApi(
              `${BASE_URL + getBasket}?UserId=${userId}`,
              "{}",
              myHeaders,
              "GET"
            );
           
            if (basket[0].code === 200) {
              console.log(basket[0]);
              dispatch(getbasketData(basket[0].data));
            }else{
            setEmptyBasket(true);

            }
         
          } catch {
            setEmptyBasket(true);
          }
        };
        GetBasket();
      };
      userProfile();
    } else {
      setEmptyBasket(true);
    }
  }, []);
  return (
    <>
      <div className="container-fluid mt-5 mb-5">
        {emptyBasket ? (
          <div className={style.basketempty}>
            <FontAwesomeIcon icon={faShoppingBasket} />
            <h4>سبد خرید شما خالی می باشد </h4>
            <Link href="/asanamooz">
              <div>
                <MainButton text="تهیه دوره ها" />
              </div>
            </Link>
          </div>
        ) : (
          <div className="row flexcenter">
            <div className={`col-lg-10 col-md-11 col-12 }`}>
              {state.loading ? (
                <Loader />
              ) : (
                <div className={`row ${style.reverse}`}>
                  <div
                    className={`col-lg-3 col-md-4 col-sm-10  ${style.center}`}
                  >
                    <ChosePort data={state.data} />
                  </div>
                  <div
                    className={`col-lg-9 col-md-8 col-sm-10 mb-3 ${style.center}`}
                  >
                    <Cart data={state.data} />
                  </div>
                </div>
              )}
            </div>

            <Steper activestep={4} />
          </div>
        )}
      </div>
    </>
  );
};

export default Payment;
