import React, { useContext, useEffect, useState } from "react";
import Countdown from "react-countdown";
import { useSelector } from "react-redux";
import daysLeft from "../../../tool/daysLeft/daysLeft";

import persianNumber from "../../../tool/persianNumber/persianNumber";
import style from "./Cart.module.css";
const Cart = ({ data }) => {
  // const data = useSelector((state) => state.statePayment.data);

  const [price, setPrice] = useState(data.classes.price);
  const [typeOfCourse, setTypeOfCourse] = useState();
  const [days, setDays] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);
  function toFarsiNumber(n) {
    const farsiDigits = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

    return n
      .toString()
      .split("")
      .map((x) => farsiDigits[x])
      .join("");
  }

  useEffect(() => {
    if (data.classes.offCode) {
      if (daysLeft(data.classes.offCode.enddate) > 0) {
        if (
          data.classes.offCode.amount !== null ||
          data.classes.offCode.amount !== 0
        ) {
          setPrice(data.classes.price - data.classes.offCode.amount);
          if (data.classes.price - data.classes.offCode.amount <= 0) {
            setPrice(0);
          }
        }
        if (
          data.classes.offCode.percent !== null ||
          data.classes.offCode.percent !== 0
        ) {
          setPrice(
            ((100 - data.classes.offCode.percent) * data.classes.price) / 100
          );
          if (
            ((100 - data.classes.offCode.percent) * data.classes.price) / 100 <=
            0
          ) {
            setPrice(0);
          }
        }
        setInterval(() => {
          var d1 = new Date(); //"now"

          var d2 = new Date("2022/05/01"); // some date

          var diff = Math.abs(d1 - d2);
          function convertToDays(milliSeconds) {
            let days = Math.floor(milliSeconds / (86400 * 1000));
            milliSeconds -= days * (86400 * 1000);
            let hours = Math.floor(milliSeconds / (60 * 60 * 1000));
            milliSeconds -= hours * (60 * 60 * 1000);
            let minutes = Math.floor(milliSeconds / (60 * 1000));
            milliSeconds -= minutes * (60 * 1000);
            let Seconds = Math.floor(milliSeconds / 1000);

            setDays(days);
            setHours(hours);
            setMinutes(minutes);
            setSeconds(Seconds);
            return {
              days,
              hours,
              minutes,
              Seconds
            };
          }
          var r = convertToDays(diff);
          if (r.days < 10) {
            r.days = "0" + r.days;
            setDays(r.days);
          }
          if (r.hours < 10) {
            r.hours = "0" + r.hours;
            setHours(r.hours);
          }
          if (r.minutes < 10) {
            r.minutes = "0" + r.minutes;
            setMinutes(r.minutes);
          }
          if (r.Seconds < 10) {
            r.Seconds = "0" + r.Seconds;
            setSeconds(r.Seconds);
          }
        }, 1000);
      } else {
        setPrice(data.classes.price);
      }
    }
    // if (data.classes.offCode !== null) {
    //   setPrice(data.classes.price - data.classes.offCode.amount);
    //   if (data.classes.price - data.classes.offCode.amount <= 0) {
    //     setPrice(0);
    //   }
    // }
    if (data.classes.type_course === "Offline") {
      setTypeOfCourse("حضوری");
    } else {
      setTypeOfCourse("غیرحضوری");
    }
  }, []);
  return (
    <>
      <div className="colStyle">
        <div className={style.course}>
          <div className={style.courseinfo}>
            <span>{data.classes.title}</span>
            <span>
              <img src="/assets/images/teacher.png" />
              {data.classes.users.name} {data.classes.users.family}
            </span>

            <span>نوع دوره: {typeOfCourse}</span>
          </div>

          <div className={style.price}>
            {data.classes.offCode !== null &&
            daysLeft(data.classes.offCode.enddate) > 0 ? (
              <>
                <span className={style.off}>
                  {persianNumber(data.classes.price)} تومان
                </span>
                <span>
                  میزان تخفیف {persianNumber(data.classes.price - price)} تومان
                  تا
                  <div className={style.timer}>
                    <div className={style.counter}>
                      <span>{toFarsiNumber(days)}</span>
                    </div>
                    <div className={style.semicolon}>:</div>
                    <div className={style.counter}>
                      <span> {toFarsiNumber(hours)}</span>
                    </div>
                    <div className={style.semicolon}>:</div>
                    <div className={style.counter}>
                      <span> {toFarsiNumber(minutes)}</span>
                    </div>
                    <div className={style.semicolon}>:</div>
                    <div className={style.counter}>
                      <span> {toFarsiNumber(seconds)}</span>
                    </div>
                  </div>
                </span>
                <span className={style.totalPrice}>
                  {persianNumber(price)}تومان
                </span>
              </>
            ) : (
              <span className={style.totalPrice}>
                {persianNumber(price)} تومان
              </span>
            )}
          </div>
          <div className={style.mobilePrice}>
            {data.classes.offCode !== null &&
            daysLeft(data.classes.offCode.enddate) > 0 ? (
              <>
                <div>
                  <span className={style.totalPrice}>
                    {persianNumber(data.classes.price)}تومان
                  </span>

                  <span className={style.off}>
                    {persianNumber(price)} تومان
                  </span>
                </div>

                <p>
                  میزان تخفیف {persianNumber(data.classes.price - price)} تومان
                  تا
                  <div className={style.timer}>
                    <div className={style.counter}>
                      <span>{toFarsiNumber(days)}</span>
                    </div>
                    <div className={style.semicolon}>:</div>
                    <div className={style.counter}>
                      <span> {toFarsiNumber(hours)}</span>
                    </div>
                    <div className={style.semicolon}>:</div>
                    <div className={style.counter}>
                      <span> {toFarsiNumber(minutes)}</span>
                    </div>
                    <div className={style.semicolon}>:</div>
                    <div className={style.counter}>
                      <span> {toFarsiNumber(seconds)}</span>
                    </div>
                  </div>
                </p>
              </>
            ) : (
              <span className={style.totalPrice}>
                {persianNumber(data.classes.price)} تومان
              </span>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default Cart;
