import Link from "next/link";
import React, { useContext, useEffect, useLayoutEffect, useState } from "react";
import SecondlyButton from "../../../tool/secondlyButton/SecondlyButton";
import style from "./ChosePort.module.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { notify } from "../../../tool/toast/toast";
import persianNumber from "../../../tool/persianNumber/persianNumber";
import { callApi } from "../../../apis/callApi";
import { BASE_URL, FindOffCode, getProfile, payment } from "../../../apis/urls";
import TabBar2 from "../../../../MobileComponents/tabBar2/TabBar2";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import daysLeft from "../../../tool/daysLeft/daysLeft";
const ChosePort = ({ data }) => {
  const router = useRouter();
  // const data = useSelector((state) => state.statePayment.data);

  const [size, setSize] = useState([0]);
  const [port, setPort] = useState(1);
  const [offCode, setOffCode] = useState();
  const [offAmount, setOffamount] = useState();
  const [preLoad, setPreLoad] = useState(false);
  const [finalPrice, setFinalPrice] = useState(data.classes.price);

  if (typeof window !== "undefined") {
    var ls = localStorage.getItem("userToken");
  }
  useEffect(() => {
    if (data.classes.offCode !== null) {
      if (daysLeft(data.classes.offCode.enddate) > 0) {
        if (
          data.classes.offCode.amount !== null ||
          data.classes.offCode.amount !== 0
        ) {
          setFinalPrice(data.classes.price - data.classes.offCode.amount);
          if (data.classes.price - data.classes.offCode.amount <= 0) {
            setFinalPrice(0);
          }
        }

        if (
          data.classes.offCode.percent !== null ||
          data.classes.offCode.percent !== 0
        ) {
          setFinalPrice(
            ((100 - data.classes.offCode.percent) * data.classes.price) / 100
          );
          if (
            ((100 - data.classes.offCode.percent) * data.classes.price) / 100 <=
            0
          ) {
            setFinalPrice(0);
          }
        }
      } else {
        setFinalPrice(data.classes.price);
      }
    }
  }, []);
  useEffect(() => {
    if (offAmount !== undefined) {
      if (finalPrice - offAmount < 0) {
        setFinalPrice(0);
      } else {
        setFinalPrice(finalPrice - offAmount);
      }
    }
  }, [offAmount]);

  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth]);
    }
    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);
  // const hamtaPort = () => {
  //   setPort("hamta");
  // };
  const idpayPort = () => {
    setPort("idPay");
  };
  const submitOffcode = () => {
    const findOffCode = async () => {
      const CheckOffCode = await callApi(
        `${BASE_URL + FindOffCode}?Code=${offCode}`,
        "{}",
        { "Content-Type": "application/json" },
        "GET"
      );
      if (CheckOffCode[0].code === 200 && CheckOffCode[0].data !== null) {
        if (
          (CheckOffCode[0].data.classid !== null &&
            data.classes.id === CheckOffCode[0].data.classid) ||
          CheckOffCode[0].data.classid === null
        ) {
          setOffamount(CheckOffCode[0].data.amount);

          // if (finalPrice - offAmount < 0) {
          //   setFinalPrice(0);
          //   console.log(finalPrice);
          document.getElementById("inputGroup-sizing-sm").disabled = true;
          const offinput = document.getElementById("offInput");
          offinput.disabled = true;
          offinput.value = "کد تخفیف ثبت شد";
          offinput.style.backgroundColor = "#e8ffe8 ";

          notify("کد تخفیف اعمال شد", "success");
        } else {
          notify("کد تخفیف اشتباه است", "error");
          setOffamount();
        }
        // } else {
        //   setFinalPrice(finalPrice - offAmount);
        // }
      } else {
        notify("کد تخفیف اشتباه است", "error");
        setOffamount();
      }
    };

    findOffCode();
  };
  const chngehndlr = (e) => {
    setOffCode(e.target.value);
  };
  const paymentHandler = () => {
    if (ls) {
      var token = JSON.parse(ls).token;
      var phone = JSON.parse(ls).phone;
    }
    setPreLoad(true);
    if (finalPrice >= 0) {
      var myHeaders = new Headers();
      myHeaders.append("Authorization", `Bearer ${token}`);
      myHeaders.append("Content-Type", "application/json");
      var raw = JSON.stringify({
        methodname: 3,
        loginWithPassword: {
          userName: phone
        }
      });
      let userProfile = async () => {
        const profile = await callApi(
          BASE_URL + getProfile,
          raw,
          myHeaders,
          "POST"
        );
        if (profile[0].code === 200) {
          const Payment = async () => {
            var raw = JSON.stringify({
              userid: profile[0].data.id,
              basket_id: data.basketId,
              amount: finalPrice * 10,
              description: ` خرید دوره  ${data.classes.title} `,
              bank: 1
            });

            const paymentResponse = await callApi(
              BASE_URL + payment,
              raw,
              myHeaders,
              "POST"
            );

            if (paymentResponse[0].code === 200) {
              window.location.href = paymentResponse[0].data.requestBank;
            }
          };
          Payment();
        }
      };
      userProfile();
    } else {
      router.push({
        pathname: `/classprograms?CourseId=${data.classes.id}`
      });
    }
  };

  return (
    <>
      <div className={`colStyle ${style.chosePort}`}>
        <div className={style.discountCode}>
          <div className="input-group input-group-sm mb-3">
            <div className="input-group-prepend">
              <button
                onClick={submitOffcode}
                className={`input-group-text ${style.submitbtn}`}
                id="inputGroup-sizing-sm"
              >
                ثبت
              </button>
            </div>
            <input
              type="text"
              className="form-control"
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
              placeholder="افزودن کد تخفیف"
              onChange={(e) => chngehndlr(e)}
              id="offInput"
            />
          </div>
        </div>
        <div className={style.port}>
          <span> انتخاب درگاه بانکی</span>

          <div className={`row ${style.allport}`}>
            <div
              className={`col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-2 ${style.portitem}`}
            >
              <div
                id="idpay"
                onClick={idpayPort}
                className={port === "idpay" && style.selectedPort}
              >
                <img src="/assets/images/idpay.svg" />
                <span>پرداخت از طریق درگاه آیدی پی</span>
              </div>
            </div>
            {/* <div
              className={`col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 mt-2 ${style.portitem}`}
            >
              <div
                id="hamta"
                onClick={hamtaPort}
                className={port === "hamta" && style.selectedPort}
              >
                <img src="/assets/images/bahamta.png" />
                <span>پرداخت از طریق درگاه با همتا</span>
              </div>
            </div> */}
          </div>
        </div>
        <div className={style.price}>
          {data.classes.offCode !== null &&
          daysLeft(data.classes.offCode.enddate) > 0 ? (
            <>
              <span className={style.off}>
                {persianNumber(
                  data.classes.price - data.classes.offCode.amount
                )}
                تومان
              </span>
              {/* <span className={style.totalPrice}>
                {persianNumber(finalPrice)}
                تومان
              </span> */}
            </>
          ) : (
            offAmount && (
              <span className={style.off}>
                {persianNumber(data.classes.price)}
              </span>
            )
          )}
          <span className={style.totalPrice}>
            {persianNumber(finalPrice)}
            تومان
          </span>
        </div>
        {/* <Link href="/paymentStatus?orderid=89"> */}
        <div onClick={paymentHandler}>
          <SecondlyButton text="پرداخت" preLoad={preLoad} />
        </div>
        {/* </Link> */}
      </div>
      {size < 500 && (
        <TabBar2
          text="پرداخت "
          coursePrice={finalPrice}
          offAmount={offAmount}
          data={data}
          preLoad={preLoad}
          page="payment"
          paymentHandler={paymentHandler}
        />
      )}
    </>
  );
};

export default ChosePort;
