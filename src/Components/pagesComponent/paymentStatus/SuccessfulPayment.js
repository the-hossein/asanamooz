import React from "react";
import Link from "next/link";
import style from "./SuccessfulPayment.module.css";
import { useRouter } from "next/router";

const SuccessfulPayment = ({ ClassId }) => {

  return (
    <>
      <div className="container">
        <div className="row center">
          <div className="col-lg-6 col-md-7 col-12 mt-5 mb-5">
            <div className={`colStyle ${style.container}`}>
              <div className="alert alert-success mt-5">
                <strong> پرداخت با موفقیت انجام شد! </strong>
              </div>
              <img src="/assets/images/success.svg" />
              {ClassId === "paid" ? (
                <Link href="/userCourses">
                  <button className={style.classProgram}>
                    مشاهده برنامه کلاسی
                  </button>
                </Link>
              ) : (
                <Link href={`/classprograms?CourseId=${ClassId}`}>
                  <button className={style.classProgram}>
                    مشاهده برنامه کلاسی
                  </button>
                </Link>
              )}
              <Link href="/">
                <button className={style.backindex}>بازگشت به صفحه اصلی</button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SuccessfulPayment;
