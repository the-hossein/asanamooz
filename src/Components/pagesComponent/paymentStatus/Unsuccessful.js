import Link from "next/link";
import React from "react";
import style from "./SuccessfulPayment.module.css";
const Unsuccessful = ({ statusCode }) => {
  const textError = "";
  switch (statusCode) {
    case 400:
      textError = "استعلام نتیجه ای نداشت.";
      break;
    case 401:
      textError = "اعتبار سنجی وب سرویس ناموفق بود.";
    case 403:
      textError = "خطا رخ داده است";
    case 404:
      textError = "وب سریس یافت نشد.";
    case 405:
      textError = "تراکنش ایجاد نشد.";
    case "catch":
      textError = "خطا در پرداخت";
    default:
      textError = "پرداخت انجام نشد";
      break;
  }
  return (
    <div className="container">
      <div className="row center">
        <div className="col-lg-6 col-md-7 col-12 mt-5 mb-5">
          <div className={`colStyle ${style.container}`}>
            <div className="alert alert-danger mt-5">
              <strong> {textError} </strong>
            </div>
            <img src="/assets/images/warning.svg" />

            <Link href="/">
              <button className={style.backindex}>بازگشت به صفحه اصلی</button>
            </Link>
            <Link href="/payment">
              <button className={style.backindex}>بازگشت به فاکتور</button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Unsuccessful;
