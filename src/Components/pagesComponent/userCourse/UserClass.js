import React from "react";
import style from "./UserClasses.module.css";
import convertDate from '../../tool/convertDate/convertDate'
import persianNumber from '../../tool/persianNumber/persianNumber'
import Link from "next/dist/client/link";
const UserClass = ({datas}) => {
  return (
    <>
      <tr>
        <td>{persianNumber(datas.basketDetailid)}</td>
        <td>{datas.title}</td>
        <td>{convertDate(datas.createdDatetime)}</td>
        <td>{persianNumber(datas.amount)}</td>
        <Link href={`/classprograms?CourseId=${datas.classId}`}>

        <td className={style.seeClass}>مشاهده کلاس</td>

        </Link>
      </tr>
    </>
  );
};

export default UserClass;
