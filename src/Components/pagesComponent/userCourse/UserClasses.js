import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUserClasses } from "../../../redux/userClasses/userClassesActions";
import { callApi } from "../../apis/callApi";
import { BASE_URL, getProfile, userClass } from "../../apis/urls";
import Loader from "../../tool/loader/Loader";
import Placement from "../../tool/placementText/Placement";
import UserClass from "./UserClass";
import style from "./UserClasses.module.css";
if (typeof window !== "undefined") {
  var ls = localStorage.getItem("userToken");
}

const UserClasses = () => {
  const state = useSelector((state) => state.stateUserClass);
  const dispatch = useDispatch();
  useEffect(() => {
    if (ls) {
      var token = JSON.parse(ls).token;
      var userID = JSON.parse(ls).userid;
      var myHeaders = new Headers();
      myHeaders.append("Authorization", `Bearer ${token}`);
      myHeaders.append("Content-Type", "application/json");

      let showuserClasses = async () => {
        const allClass = await callApi(
          `${BASE_URL + userClass}?UserId=${userID}`,
          "{}",
          myHeaders,
          "GET"
        );
        if (allClass[0].code === 200) {
          dispatch(getUserClasses(allClass[0].data));
        }
      };
      showuserClasses();
    } else {
      dispatch(getUserClasses([]));
    }
  }, []);
  return (
    <>
      <div className={style.userClass}>
        <div className="colStyle">
          <p className={style.title}>دوره های شما</p>
          <div className={style.tableprogram}>
            {state.loading ? (
              <Loader />
            ) : (
              <table className={`table table-borderless`}>
                <thead>
                  <tr>
                    <th>شماره فاکتور </th>
                    <th>نام دوره</th>
                    <th>تاریخ </th>
                    <th>مبلغ دوره </th>
                    <th> عملیات </th>
                  </tr>
                </thead>

                <tbody>
                  {state.data.map((item) => (
                    <>
                      <UserClass datas={item} />
                    </>
                  ))}
                </tbody>
              </table>
            )}
            {state.data.length === 0 && !state.loading && (
              <Placement text="دوره ای یافت نشد" />
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default UserClasses;
