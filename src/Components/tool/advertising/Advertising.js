import React, { useEffect, useLayoutEffect, useState } from "react";
import Image from "next/image";
// import advDesktop from "../../../../public/assets/images/ramedanadv1.jpg";
// import advMobile from "../../../../public/assets/images/ramedanadv2.jpg";
import style from "./Advertising.module.css";
import Link from "next/link";
import persianNumber from "../persianNumber/persianNumber";
const Advertising = () => {
  const [days, setDays] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);
  const [size, setSize] = useState([0]);
  function toFarsiNumber(n) {
    const farsiDigits = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

    return n
      .toString()
      .split("")
      .map((x) => farsiDigits[x])
      .join("");
  }
  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth]);
    }
    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);
  useEffect(() => {
    setInterval(() => {
      var d1 = new Date(); //"now"

      var d2 = new Date("2022/05/01"); // some date

      var diff = Math.abs(d1 - d2);
      function convertToDays(milliSeconds) {
        let days = Math.floor(milliSeconds / (86400 * 1000));
        milliSeconds -= days * (86400 * 1000);
        let hours = Math.floor(milliSeconds / (60 * 60 * 1000));
        milliSeconds -= hours * (60 * 60 * 1000);
        let minutes = Math.floor(milliSeconds / (60 * 1000));
        milliSeconds -= minutes * (60 * 1000);
        let Seconds = Math.floor(milliSeconds / 1000);

        setDays(days);
        setHours(hours);
        setMinutes(minutes);
        setSeconds(Seconds);
        return {
          days,
          hours,
          minutes,
          Seconds
        };
      }
      var r = convertToDays(diff);
      if (r.days < 10) {
        r.days = "0" + r.days;
        setDays(r.days);
      }
      if (r.hours < 10) {
        r.hours = "0" + r.hours;
        setHours(r.hours);
      }
      if (r.minutes < 10) {
        r.minutes = "0" + r.minutes;
        setMinutes(r.minutes);
      }
      if (r.Seconds < 10) {
        r.Seconds = "0" + r.Seconds;
    
        setSeconds(r.Seconds);
      }
    }, 1000);
    // setInterval(() => {
    //   const now = moment();
    //   const then = moment("2022-05-01", "YYYY-MM-DD");
    //   const countdown = moment(then - now);
    //   setDays(countdown.format("D"));
    //   setHours(countdown.format("HH"));
    //   setMinutes(countdown.format("mm"));
    //   setSeconds(countdown.format("ss"));
    // }, 1000);
  }, []);

  return (
    <>
      <Link href="/asanamooz">
        <div className={`col-lg-12  col-12 `}>
          <div className={style.adv}>
            <div className={style.timer}>
              <div className={style.counter}>
                <span>{toFarsiNumber(days)}</span>
                <span>روز</span>
              </div>
              <div className={style.semicolon}>:</div>
              <div className={style.counter}>
                <span> {toFarsiNumber(hours)}</span>
                <span>ساعت</span>
              </div>
              <div className={style.semicolon}>:</div>
              <div className={style.counter}>
                <span> {toFarsiNumber(minutes)}</span>
                <span>دقیقه</span>
              </div>
              <div className={style.semicolon}>:</div>
              <div className={style.counter}>
                <span> {toFarsiNumber(seconds)}</span>
                <span>ثانیه</span>
              </div>
            </div>
            {size > 570 ? (
              // <Image src={advDesktop} alt="ramezan" />
              <img
                className={style.advImg}
                src="/assets/images/ramedanadv1.jpg"
              />
            ) : (
              // <Image src={advMobile} alt="ramezan" />
              <img
                className={style.advImg}
                src="/assets/images/ramedanadv2.jpg"
              />
            )}
          </div>
        </div>
      </Link>
    </>
  );
};
export default Advertising;
