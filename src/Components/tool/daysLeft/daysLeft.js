const daysLeft = (date) => {

  const now = new Date();
  const endDate = new Date(date);

  return endDate - now;
};

export default daysLeft;
