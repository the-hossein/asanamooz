import React from 'react';
import style from './Loader.module.css'
const Loader = () => {
  return <>
  <div className={`text-center mt-3 mb-3 ${style.loader}`}>
  <div className="spinner-border text-info" role="status">
    <span className="sr-only"></span>
  </div>
</div>
  
  
  </>;
};

export default Loader;
