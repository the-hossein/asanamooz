import React from "react";
import style from "./MainButton.module.css";
const MainButton = ({ text, href, preLoad }) => {
  return (
    <>
      <a className={style.mainbtn} href={href}>
        
        {text}
        {preLoad && (
          <div className="spinner-border text-light" role="status">
            <span className="sr-only"></span>
          </div>
        )}
      </a>
    </>
  );
};

export default MainButton;
