import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import Course from '../../course/Course'
import Placment from "../placementText/Placement";
import style from "./Paginate.module.css";
const Paginate = ({ perpage, datas}) => {
  const [coursedata, setCourse] = useState([]);
  const [pageNumber, setPageNumber] = useState(0);
// useEffect(()=>{
// setPreload(false)
// },[])
  useEffect(() => {
    document.getElementById("animate").classList.add(style.transition);
  }, [pageNumber]);
  const perPage = perpage;
  const pageVisited = pageNumber * perPage;
  const displayCourse = datas
    .slice(pageVisited, pageVisited + perPage)

    .map((item) => {
      return (
        <>
          <div className="col-xl-4  col-6">
            <Course
            key={item.id}
         data={item}
            />
          </div>
        </>
      );
    });
  {
  }
  const chngePage = ({ selected }) => {
    setPageNumber(selected);
    const test = document.getElementsByClassName(style.transition);
    for (let i = 0; i < test.length; i++) {
      test[i].classList.remove(style.transition);
    }
  };
  const pageCount = Math.ceil(datas.length / perPage);
  return (
    <>
      <div id="animate" className={"row"}>
        { displayCourse.length===0?
        <Placment text="هیچ دوره ای نیست"/>
        :
          displayCourse
        }
      </div>
      <ReactPaginate
        previousLabel="صفحه قبلی"
        nextLabel="صفحه بعد"
        pageCount={pageCount}
        onPageChange={chngePage}
        pageRangeDisplayed={2}
        marginPagesDisplayed={1}
        renderOnZeroPageCount={null}
        containerClassName={style.pagination}
        previousLinkClassName={style.prevbtn}
        nextLinkClassName={style.nextbtn}
        disabledClassName={style.disablebtn}
        activeClassName={style.PaginationActive}
      />
    </>
  );
};

export default Paginate;
