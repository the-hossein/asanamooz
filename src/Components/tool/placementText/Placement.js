import React from 'react';
import style from './Placement.module.css'
const Placement = ({text}) => {
  return <>
  <div  className={style.container}>
      {text}
  </div>
  </>;
};

export default Placement;
