import React from "react";
import style from "./SecondlyButton.module.css";
const SecondlyButton = ({ text, preLoad,clickHandler}) => {

  return (
    <>
      <button className={style.secondlyButton} onClick={clickHandler}>
       
        {preLoad ? (
          <div
            className={`spinner-border text-light ${style.sppiner}`}
            role="status"
          >
            <span className="sr-only"></span>
          </div>
        ): text}
      </button>
    </>
  );
};
export default SecondlyButton;
