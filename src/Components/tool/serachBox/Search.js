import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import React, { useState } from "react";
import style from "./Search.module.css";

const Search = ({setSearchBox}) => {
  const [search, setSearch] = useState();
  const searchChange = (e) => {
    setSearch(e.target.value);
  };
const closeBox=()=>{

 setSearchBox(false)
}
  return (
    <>
      <div id="showSearchBox" className={style.searchBox} >
        <div id={style.search}>
          <span className={style.close} onClick={closeBox}>
            <img src="/assets/images/close.png" />
          </span>
          <input
            className="form-control"
            value={search}
            onChange={searchChange}
          />
          <span >
            <FontAwesomeIcon icon={faSearch}   />
          </span>
        </div>
      </div>
    </>
  );
};

export default Search;
