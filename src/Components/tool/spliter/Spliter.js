import React from "react";
import style from "./Spliter.module.css";
export const Spliter = ({ textSpliter }) => {
  return (
    <>
      <div className={`col-10 paddig0  mt-4 mb-4 ${style.spliter}`}>
        <div></div>
        <span>{textSpliter}</span>
      </div>
    </>
  );
};
