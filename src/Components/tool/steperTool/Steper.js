import Link from "next/link";
import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import style from "./Steper.module.css";
const Steper = ({ activestep }) => {
  useEffect(() => {
    for (let i = activestep; i > 0; i--) {
      const actived = document.getElementById(`${i}`);
      actived.classList.add(style.active);
      actived.nextElementSibling.classList.add(style.activeTitle);
    }
  }, []);
  const state = useSelector((state) => state.stateBuyCourse);
  return (
    <>
      <div className={`col-lg-1 col-1  ${style.displaySteper}`}>
        <div className={`colStyle ${style.stepper} `}>
          <div>
            <Link href="/">
              <div className={style.stepperHover}>
                <div id={1}className={style.stepperShape} ></div>
                <span>آسان آموز</span>
              </div>
            </Link>
            <Link href="/asanamooz">
              <div className={style.stepperHover}>
                <div id={2}className={style.stepperShape}  ></div>
                <span>انتخاب دوره</span>
              </div>
            </Link>
            {activestep === 4 ? (
              <Link href={`/buyCourse/${state.data.id}`}>
                <div className={style.stepperHover}>
                  <div id={3} className={style.stepperShape} ></div>
                  <span>خرید دوره</span>
                </div>
              </Link>
            ) : (
              <div>
                <div id={3}className={style.stepperShape} ></div>
                <span>خرید دوره</span>
              </div>
            )}
            <div>
              <div id={4}className={style.stepperShape} ></div>
              <span>پرداخت</span>
            </div>
            <div>
              <div id={5}className={style.stepperShape} ></div>
              <span>مشاهده برنامه </span>
            </div>
            <div>
              <div className={style.stepperShape} ></div>
              <span>پرینت برنامه</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Steper;
