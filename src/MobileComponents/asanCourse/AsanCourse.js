import Link from "next/link";
import React, { useEffect, useState } from "react";
import Countdown from "react-countdown";
import daysLeft from "../../Components/tool/daysLeft/daysLeft";
import persianNumber from "../../Components/tool/persianNumber/persianNumber";
import { notify } from "../../Components/tool/toast/toast";
import style from "./AsanCourse.module.css";
const AsanCourse = ({ data }) => {
  const [offPrice, setOffPrice] = useState(0);
  useEffect(() => {
    if (data.offCodePercent !== 0) {
      let result = ((100 - data.offCodePercent) * data.totalPrice) / 100;
      setOffPrice(result);
    }
    if (data.offCodeAmount !== 0) {
      setOffPrice(data.totalPrice - data.offCodeAmount);
    }
    console.log(offPrice);
  }, []);
  return (
    <>
      <Link href={data.isEnable === true ? `/buyCourse/${data.classID}` : "/asanamooz"}>
        <div 
        
        className={
          data.isEnable === false
            ? `${style.container} ${style.disableCourse}`
            : style.container
        }
        onClick={()=>data.isEnable===false&&notify("دوره غیر فعال می باشد","warning")}
        >
          <div className={style.asanCourse}>
            <div>
              <div className={style.courseContent}>
                <h4>{data.className}</h4>
                <div className={style.courseTeacher}>
                  <img src="/assets/images/teacher32.png" />
                  <span>
                    {data.teacherName} {data.teacherFamily}
                  </span>
                </div>
                <div className={style.courseTime}>
                  <img src="/assets/images/duration24.png" />
                  <span>
                    مدت زمان کلاس : {persianNumber(data.duration)} ساعت
                  </span>
                </div>
              </div>
              {data.offCodeAmount !== 0 ||data.offCodePercent!==0? (
                <div className={style.specialoffDisplay}>
                  <div className={style.off}>
                    <div>
                      <span>{persianNumber(data.totalPrice)} تومان</span>
                      <span>
                        {persianNumber(offPrice)}
                        تومان
                      </span>
                    </div>
                    <div className={style.offRate}>
                      <img src="/assets/images/off.png" />
                      <div>
                        <span>{data.offCodePercent}%</span>
                        {/* <Countdown
                          date={Date.now() + daysLeft(data.endDate)}
                          autoStart={true}
                        /> */}
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <div className={style.Courseprice}>
                  <div className={style.price}>
                    {data.totalPrice === "0" ? (
                      <span>رایگان</span>
                    ) : (
                      <span> {persianNumber(data.totalPrice)} تومان</span>
                    )}
                  </div>
                </div>
              )}
            </div>
            <img src={data.filePath} />
          </div>
        </div>
      </Link>
    </>
  );
};

export default AsanCourse;
