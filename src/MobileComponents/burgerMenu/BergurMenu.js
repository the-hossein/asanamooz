import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { callApi } from "../../Components/apis/callApi";
import { BASE_URL, getProfile } from "../../Components/apis/urls";
import { alldataUser, userNotLogined } from "../../redux/login/loginActions";
import style from "./BergurMenu.module.css";
const BergurMenu = ({ setShowmenu }) => {
  const state=useSelector(state=>state.stateLogin)
  const dispatch=useDispatch()
  const [username, setUserName] = useState({
    name: "",
    phone: ""
  });
  if (typeof window !== "undefined") {
    var ls = localStorage.getItem("userToken");
  }
  const closeMenu = () => {
    setShowmenu(false);
  };
  useEffect(() => {
    if (ls) {
  dispatch(alldataUser())
    } else {
     dispatch(userNotLogined())
    }
  }, []);
  const logout = () => {
    localStorage.removeItem("userToken");
    location.reload();
  };
  return (
    <>
      <div className={style.container} onClick={closeMenu}>
        <div className={style.menuBar}>
          <div className={style.menuHeader}>
            <span className={style.closemenu} onClick={closeMenu}>
              X
            </span>
            <div className={style.logo}>
              <img src="/assets/images/logo.png" />
            </div>
            {
              state.loginStatus?


            <div>
                <>
                 <p>
            {state.userData.name !== "نامشخص" ||
                      state.userData.family !== "نامشخص"
                        ? state.userData.name + " " + state.userData.family
                        : state.phoneNumber}


                 </p>
                  <Link href="/userCourses">
                    <p className={style.yourCourses}>دوره های شما</p>
                  </Link>
                    <p onClick={logout}>خروج</p>
               
              </>
            </div>
            :
            ""
            }
          </div>
          <div className={style.menuItem}>
            <ul>
              <li>
                <a href="https://asanbtc.com/">
                  <img src="/assets/images/002-home.png" />
                  صفحه اصلی
                </a>
              </li>
              <li>
                <a href="https://asanbtc.com/Contents?typeid=1#MainContent_UpdateContent">
                  <img src="/assets/images/002-news.png" /> اخبار
                </a>
              </li>
              <li>
                <a href="https://asanbtc.com/AllCoins">
                  <img src="/assets/images/005-bitcoin-logo.png" />
                  قیمت ارزها
                </a>
              </li>
              <li>
                <a href="https://asanbtc.com/Exchanges">
                  <img src="/assets/images/003-currency.png" />
                  صرافی ها
                </a>
              </li>
              <li>
                <a href="https://asanbtc.com/AllCoins">
                  <img src="/assets/images/010-payment-method-1.png" />
                  خرید و فروش
                </a>
              </li>
              <li>
                <Link href="/asanamooz">
                  <a>
                    <img src="/assets/images/003-cap.png" />
                    آسان آموز
                  </a>
                </Link>
              </li>
              <li>
                <a href="https://asanbtc.com/Consulting">
                  <img src="/assets/images/001-consultation.png" />
                  مشاوره
                </a>
              </li>
              <li>
                <a href="https://asanbtc.com/About">
                  <img src="/assets/images/001-info.png" />
                  درباره ما
                </a>
              </li>
              <li>
                <a href="https://asanbtc.com/Service">
                  <img src="/assets/images/003-global-services.png" />
                  خدمات
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

export default BergurMenu;
