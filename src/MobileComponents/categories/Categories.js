import React, { useEffect } from "react";
import SecondlyButton from "../../Components/tool/secondlyButton/SecondlyButton";
import style from "./Categories.module.css";
const Categories = ({ setCategory, setChoseCategory,choseCategory }) => {
  useEffect(() => {
    const radioitems = document.querySelectorAll(".form-check-input");
    for (let i = 0; i < radioitems.length; i++) {
      if (radioitems[i].getAttribute("value") ===choseCategory ) {
        radioitems[i].checked = true;
      }
    }

  }, []);
  const addCategory = () => {
    setCategory(false);
  };
  const choseItem = (e) => {
    setChoseCategory(e.target.value);
  };
  return (
    <>
      <div className={`colStyle ${style.allCategoriy}`}>
        <p> دسته بندی ها</p>
        <div>
          <label className="form-check-label">
            <input
              onClick={choseItem}
              className="form-check-input"
              name="categories"
              type="radio"
              value="all"
              id="all"
            />
            همه
          </label>
          <label className="form-check-label">
            <input
              onClick={choseItem}
              className="form-check-input"
              name="categories"
              type="radio"
              value="beg"
              id="begginer"
            />
            دوره مبتدی
          </label>
          <label className="form-check-label">
            <input
              onClick={choseItem}
              className="form-check-input"
              name="categories"
              type="radio"
              value="pro"
              id="advanced"
            />
            دوره پیشرفته
          </label>
          <label className="form-check-label">
            <input
              onClick={choseItem}
              className="form-check-input"
              name="categories"
              type="radio"
              value="free"
              id="free"
            />
            دوره رایگان
          </label>
          <label className="form-check-label">
            <input
              onClick={choseItem}
              className="form-check-input"
              name="categories"
              type="radio"
              value="off"
              id="off"
            />
            دوره با تخفیف
          </label>
        </div>
        <div onClick={addCategory}>
          <SecondlyButton text="اعمال دسته بندی" />
        </div>
      </div>
    </>
  );
};

export default Categories;
