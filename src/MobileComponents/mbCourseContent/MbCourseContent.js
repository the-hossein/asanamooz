import React, { useEffect, useState } from "react";
import Alltitle from "../../Components/pagesComponent/buyCourse/allTitle/Alltitle";
import DateTime from "../../Components/pagesComponent/buyCourse/courseContent/dateTimeCourse/DateTime";
import Placement from "../../Components/tool/placementText/Placement";
import style from "./MbCourseContent.module.css";
import Scroll from "react-scroll";

const MbCourseContent = ({ data }) => {
  var Link = Scroll.Link;
  var Element = Scroll.Element;
  var Events = Scroll.Events;
  var scroll = Scroll.animateScroll;
  var scrollSpy = Scroll.scrollSpy;
  const [tab, setTab] = useState("intro");
  const selectTab = (e) => {
    //e.target.children.classList.toggle(style.displayContent)
    setTab(e.target.getAttribute("id"));
    // e.target.nextElementSibling.classList.toggle(style.displayContent);
  };

  useEffect(() => {
    Events.scrollEvent.register("begin", function () {
      console.log("begin", arguments);
    });

    Events.scrollEvent.register("end", function () {
      console.log("end", arguments);
    });

    scrollSpy.update();
  }, []);
  const scrollToTop = () => {
    scroll.scrollToTop();
  };
  const componentWillUnmount = () => {
    Events.scrollEvent.remove("begin");
    Events.scrollEvent.remove("end");
  };
  const descriptions = () => {
    return { __html: data.description };
  };
  const topic = () => {
    return { __html: data.topics };
  };
  return (
    <>
      <div className={style.courseContent}>
        <Link
          activeClass="active"
          to="introText"
          spy={true}
          smooth={true}
          duration={250}
          containerId="containerElement"
        >
          <Element name="placeTimes"  style={{marginTop:"2rem"}}>
              <Element name="topicc" style={{marginTop:"2rem"}}>
            <div onClick={selectTab} className={style.tabs} >
              <span id="intro" className={tab === "intro" && style.active}>
                معرفی دوره آموزشی
              </span>
              {tab === "intro" && (
                <div>
                  {data.description === "" ? (
                    <Placement text="هیچ محتوایی نیست" />
                    ) : (
                      <Element name="introText" >
                      <p
                        id={style.des}
                        dangerouslySetInnerHTML={descriptions()}
                        ></p>
                    </Element>
                  )}
                </div>
              )}
            </div>
              </Element>
          </Element>
        </Link>
        <Link
          activeClass="active"
          to="placeTimes"
          spy={true}
          smooth={true}
          duration={250}
          containerId="containerElement"
        >
          <div onClick={selectTab} className={style.tabs}>
            <span id="place" className={tab === "place" && style.active}>
              مکان و زمان دوره
            </span>
            {tab === "place" && <DateTime data={data} />}
          </div>
        </Link>
        <Link
          activeClass="active"
          to="topicc"
          spy={true}
          smooth={true}
          duration={250}
          containerId="containerElement"
        >
          <div onClick={selectTab} className={style.tabs}>
            <span id="titles" className={tab === "titles" && style.active}>
              سرفصل های دوره
            </span>
            {
              tab === "titles" && (
                  <div
                    id={style.topics}
                    dangerouslySetInnerHTML={topic()}
                  ></div>
              )
              // <Alltitle />
            }
          </div>
        </Link>
      </div>
    </>
  );
};

export default MbCourseContent;
