import {
  faArrowDown,
  faChevronDown,
  faLock
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import Loader from "../../Components/tool/loader/Loader";
import MainButton from "../../Components/tool/mainButton/MainButton";
import persianNumber from "../../Components/tool/persianNumber/persianNumber";
import Placement from "../../Components/tool/placementText/Placement";
import styles from "./MobileClassProgram.module.css";
const MobileClassProgram = ({ course, record }) => {
  // console.log(course);
  // const [session, setSession] = useState("جلسه اول");
  // const selectHndler = (e) => {
  //   if (e.target.classList.contains("session")) {
  //     setSession(e.target.getAttribute("value"));
  //   }
  // };
  // const openBox = () => {
  //   document
  //     .querySelector(`.${styles.selectitem}`)
  //     .classList.toggle(styles.displayBox);
  // };

  return (
    <>
      <div>
        <div className={`colStyle ${styles.classprogram}`}>
          <p>برنامه کلاسی دوره های آموزشی آسان بیت کوین</p>
          <div>
            <ul>
              <li> {course.title}</li>
              <li>
                <img src="/assets/images/teacher.png" /> {course.users.name}{" "}
                {course.users.family}
              </li>
              <li> تعداد جلسات دوره : {course.session_count}</li>
              <li>آدرس کلاس: {course.location}</li>
            </ul>
          </div>
        </div>
        {/* <div className={styles.sessionSelection}>
            <span>انتخاب جلسه</span>
            <div className={styles.selected} onClick={openBox}>
              <span>{session}</span> <FontAwesomeIcon icon={faChevronDown} />
              <ul className={styles.selectitem}>
                {record.map((item) => (
                  <>
                    {" "}
                    <li
                      value={`${item.title}`}
                      onClick={selectHndler}
                      className="session"
                    >
                      {item.title}{" "}
                      <img src="/assets/images/icons8-lock-16.png" />
                    </li>
                  </>
                ))}
              </ul>
            </div>
          </div> */}

        <div className={`colStyle ${styles.session}`}>
          <div>
            {record.length <= 0 ? (
              <Placement text="برنامه ای نیست" />
            ) : (
              record.map((item) => (
                <>
                  <p>{item.title}</p>
                  <div>
                    <span>تاریخ کلاس</span>
                    <span>
                      {new Date(
                        item.startDatetime.split("T")[0]
                      ).toLocaleDateString("fa-IR")}
                    </span>
                  </div>
                  <div>
                    <span>ساعت کلاس</span>
                    <span>{item.holdingTime} </span>
                  </div>
                  <div>
                    <span>وضعیت کلاس</span>
                    <span
                      className={
                        item.stateType.title === "اتمام کلاس"
                          ? styles.finished
                          : item.stateType.title === "برگزار نشد"
                          ? styles.notheld
                          : styles.onperforming
                      }
                    >
                      {item.stateType.title}
                    </span>
                  </div>
                </>
              ))
            )}
          </div>
        </div>
        <div className={`colStyle ${styles.evidence}`}>
          <p> پرینت برنامه ی کلاسی</p>
          <MainButton text="پرینت برنامه" />
        </div>
      </div>
    </>
  );
};

export default MobileClassProgram;
