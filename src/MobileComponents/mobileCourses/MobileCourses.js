import style from "./MobileCourses.module.css";
import AsanCourse from "../asanCourse/AsanCourse";
import { useEffect, useState } from "react";
import MobileFilters from "../mobileFilters/MobileFilters";
import Categories from "../categories/Categories";
import { Spliter } from "../../Components/tool/spliter/Spliter";
import ConsultationRequest from "../../Components/pagesComponent/landing/consultationRequest/ConsultationRequest";
import Placement from "../../Components/tool/placementText/Placement";
import { useSelector } from "react-redux";
const MobileCourses = () => {
  const satate = useSelector((state) => state.stateAsanAmoz);
  const [filter, setFilter] = useState(false);
  // const [category, setCategory] = useState(false);
  const [filterData, setFilterData] = useState(satate.allCourses);
  // const [teacher, setTeacher] = useState(null);
  const [level, setLevel] = useState(null);
  const [price, setPrice] = useState(null);
  // const [choseCategory, setChoseCategory] = useState("all");
  const [filterItem, setFilterItem] = useState(filterData);
  useEffect(() => {
    if (level !== null) {
      setFilterItem(
        filterData.filter((item) => {
          if (level === "all") {
            return item;
          } else {
            return item.courseLevel === level;
          }
        })
      );
    }
    if (price !== null) {
      setFilterItem(
        filterData.filter((item) => {
          if (price === "all") {
            return item;
          } else {
            if (price === "off") {
              return item.offCodeAmount !== 0;
            }
            if (price === "0") {
              return item.totalPrice === 0;
            }
          }
        })
      );
    }
    if (level !== null && price !== null) {
      setFilterItem(
        filterData.filter((item) => {
          if (level === "all" && price === "all") {
            return item;
          }
          if (level !== null && price === "all") {
            return item.courseLevel === level;
          }
          if (level === "all" && price === "off") {
            return item.offCodeAmount !== 0;
          }
          if (level !== null && price === "off") {
            return item.offCodeAmount !== 0 && item.courseLevel === level;
          }
          if (level !== null && price === "free") {
            if (level === "all") {
              return item.totalPrice === 0;
            }
            return item.totalPrice === 0 && item.courseLevel === level;
          }
        })
      );
    }
  }, [level, price]);
  // useEffect(() => {
  //   if (choseCategory === "all") {
  //     setFilterItem(filterData);
  //     if (level !== null) {
  //       setFilterItem(
  //         filterData.filter((item) => {
  //           if (level === "all") {
  //             return item;
  //           } else {
  //             return item.courseLevel === level;
  //           }
  //         })
  //       );
  //     }
  //     if (price !== null) {
  //       setFilterItem(
  //         filterData.filter((item) => {
  //           if (price === "all") {
  //             return item;
  //           } else {
  //             return item.totalPrice <= price;
  //           }
  //         })
  //       );
  //     }
  //     if (teacher !== null) {
  //       setFilterItem(
  //         filterData.filter((item) => item.teacherFamily === teacher)
  //       );
  //       if (teacher === "all") {
  //         setFilterItem(filterData);
  //       }
  //     }
  //     if (level !== null && price !== null) {
  //       setFilterItem(
  //         filterData.filter((item) => {
  //           if (level === "all" && price === "all") {
  //             return item;
  //           } else {
  //             if (level === "all") {
  //               return item && item.totalPrice <= price;
  //             }
  //           }
  //         })
  //       );
  //     }
  //   }
  //   if (choseCategory === "begginer") {
  //     setFilterItem(filterData.filter((item) => item.courseLevel === "beg"));
  //     if (level !== null) {
  //       setFilterItem(filterData.filter((item) => item.courseLevel === level));
  //       if (level === "all") {
  //         setFilterItem(filterData);
  //       }
  //     }
  //   }
  // }, [teacher, level, price, choseCategory]);

  // const showCategoryBox = () => {
  //   setFilter(false);
  //   setCategory(!category);
  // };
  const showFilterBox = () => {
    // setCategory(false);
    setFilter(!filter);
  };
  return (
    <>
      <div className={style.mobileCourse}>
        <div className={style.dropboxTab}>
          <div>
            <div className={style.filters} onClick={showFilterBox}>
              <img src="/assets/images/filters.png" alt="filters image" />
              فیلترها
            </div>
            {/* <div className={style.categories} onClick={showCategoryBox}>
              <img src="/assets/images/categories.png" alt="categories image" />
              دسته بندی ها
            </div> */}
          </div>
        </div>
        <div className={style.allCourse}>
          {filter ? (
            <MobileFilters
              setFilter={setFilter}
              level={level}
              setLevel={setLevel}
              // teacher={teacher}
              // setTeacher={setTeacher}
              price={price}
              setPrice={setPrice}
            />
          ) : // ) : category ? (
          //   <Categories
          //     setCategory={setCategory}
          //     setChoseCategory={setChoseCategory}
          //     choseCategory={choseCategory}
          //   />
          // ) : filterItem.length <= 0 ? (
          //   <Placement text="هیچ دوره ای نیست" />
          // ) : (
          //   filterItem.map((item) => (
          //     <>
          //       <AsanCourse data={item} key={item.classID} />
          //     </>
          //   ))
          // )}
          filterItem.length == 0 ? (
            <Placement text="هیچ دوره ای نیست" />
          ) : (
            filterItem
              .sort((a, b) => Number(b.isEnable) - Number(a.isEnable))
              .map((item) => (
                <>
                  <AsanCourse data={item} key={item.classID} />
                </>
              ))
          )}

          <Spliter textSpliter="درخواست مشاوره" />
          <ConsultationRequest />
        </div>
      </div>
    </>
  );
};

export default MobileCourses;
