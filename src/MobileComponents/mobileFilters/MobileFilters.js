import { faChevronDown, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState, useEffect } from "react";
import SecondlyButton from "../../Components/tool/secondlyButton/SecondlyButton";
import style from "./MobileFilters.module.css";
const MobileFilters = ({ setFilter, level, setLevel, price, setPrice }) => {
  useEffect(() => {
    // if(teacher!==null){

    //  document.getElementById(`${teacher}`).checked=true
    // }
    if (level !== null) {
      document.getElementById(`${level}`).checked = true;
    }
    if (price !== null) {
      document.getElementById(`${price}`).checked = true;
    }
    if (price === "all") {
      document.getElementById(`all`).checked = true;
    }
  }, []);
  // const openTeacherName = () => {
  //   document.getElementById("byName").classList.toggle(style.show);
  // };
  const openLevel = () => {
    document.getElementById("bylevel").classList.toggle(style.show);
  };
  const openPrice = () => {
    document.getElementById("byprice").classList.toggle(style.show);
  };
  const addFilter = () => {
    setFilter(false);
  };
  const radioHndler = (e) => {
    switch (e.target.name) {
      // case "teachrname":
      //   setTeacher(e.target.value);
      //   break;
      case "levelcourse":
        setLevel(e.target.value);

        break;
      case "priceCourse":
        setPrice(e.target.value);

        break;
      default:
        break;
    }
  };
  return (
    <>
      <div className={`colStyle ${style.filterMobile}`}>
        <p className={style.title}> فیلتر ها</p>
        <div className={style.filters}>
          <ul>
            {/* <li onClick={openTeacherName}>
              <div className="name">
                نام مدرس
                <FontAwesomeIcon icon={faPlus} />
              </div>
            </li>
            <div className={style.subitem} id="byName">
              <label className="form-check-label">
                <input
                  className="form-check-input"
                  onClick={radioHndler}
                  name="teachrname"
                  type="radio"
                  value="all"
                  id="all"
                />
                همه
              </label>
              <label className="form-check-label">
                <input
                  className="form-check-input"
                  onClick={radioHndler}
                  name="teachrname"
                  type="radio"
                  value="احمدزادگان"
                  id="احمدزادگان"
                />
                مهندس حمید احمدزادگان
              </label>
              <label className="form-check-label">
                <input
                  className="form-check-input"
                  onClick={radioHndler}
                  name="teachrname"
                  type="radio"
                  value="خاکبازان"
                  id="خاکبازان"
                />
                مهندس مسعود خاکبازان
              </label>
            </div> */}
            <li onClick={openLevel}>
              <div className="level">
                سطح دوره
                <FontAwesomeIcon icon={faPlus} />
              </div>
            </li>
            <div className={style.subitem} id="bylevel">
              <label className="form-check-label">
                <input
                  className="form-check-input"
                  onClick={radioHndler}
                  name="levelcourse"
                  type="radio"
                  value="all"
                  id="all"
                />
                همه
              </label>
              <label className="form-check-label">
                <input
                  className="form-check-input"
                  onClick={radioHndler}
                  name="levelcourse"
                  type="radio"
                  value="مقدماتی"
                  id="مقدماتی"
                />
                مبتدی
              </label>
              <label className="form-check-label">
                <input
                  className="form-check-input"
                  onClick={radioHndler}
                  name="levelcourse"
                  type="radio"
                  value="پیشرفته"
                  id="پیشرفته"
                />
                پیشرفته
              </label>
            </div>
            {/* <li onClick={openPrice}>
              <div className="price">
                قیمت دوره
                <FontAwesomeIcon icon={faPlus} />
              </div>
            </li>
            <div className={style.subitem} id="byprice">
              <label className="form-check-label">
                <input
                  className="form-check-input"
                  onClick={radioHndler}
                  name="priceCourse"
                  type="radio"
                  value="all"
                  id="all"
                />
                همه
              </label>
              <label className="form-check-label">
                <input
                  className="form-check-input"
                  onClick={radioHndler}
                  name="priceCourse"
                  type="radio"
                  value="off"
                  id="off"
                />
                تخفیف ویژه
              </label>
              <label className="form-check-label">
                <input
                  className="form-check-input"
                  onClick={radioHndler}
                  name="priceCourse"
                  type="radio"
                  value="free"
                  id="free"
                />
                رایگان
              </label>
            </div> */}
          </ul>
        </div>
        <div onClick={addFilter}>
          <SecondlyButton text="اعمال فیلتر" />
        </div>
      </div>
    </>
  );
};

export default MobileFilters;
