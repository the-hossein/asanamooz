import Link from "next/link";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Getphone from "../../Components/getPhoneUser/Getphone";
import { showPopup, statusGetPhone } from "../../redux/getphone/getphoneAction";
import style from "./TabBar.module.css";

const TabBar = () => {
  const dispatch = useDispatch();
  // useEffect(() => {
  //   window.$crisp = [];
  //   window.CRISP_WEBSITE_ID = "448930c2-88fb-4cd7-9d1e-5a5084927352";
  //   (() => {
  //     const d = document;
  //     const s = d.createElement("script");
  //     s.src = "https://client.crisp.chat/l.js";
  //     s.async = 1;
  //     d.getElementsByTagName("body")[0].appendChild(s);
  //   })();
  // }, []);
  const chatOnine = () => {
    document.querySelector(".cc-unoo").click();
  };
  const router = useRouter();
  const [popup, setPopUp] = useState(false);
  const state = useSelector((state) => state.stateLogin);
  const registerHandler = () => {
    dispatch(showPopup());
  };
  const request = () => {
    dispatch(statusGetPhone("support"))
    dispatch(showPopup());
  };
  if (typeof window !== "undefined") {
    var ls = localStorage.getItem("userToken");
  }
  const carHandler = () => {
    if (state.loginStatus) {
      router.push({
        pathname: "/payment"
      });
    } else {
      dispatch(showPopup());

    }
  };

  return (
    <>
      <div className={style.tabBar}>
        <div>
          <Link href="/">
            <div className={style.tab}>
              <img src="/assets/images/001-home.png" /> صفحه اصلی
            </div>
          </Link>
          <Link href="/asanamooz">
            <div className={style.tab}>
              <img src="/assets/images/002-open-book.png" /> دوره ها
            </div>
          </Link>

          <div
            className={`${style.contactUs} ${style.tab}`}
            id="test"
            onClick={chatOnine}
          >
            <img src="/assets/images/001-phone-call.png" />
          </div>

          {state.loginStatus ? (
            <div onClick={request} className={style.tab}>
              <img src="/assets/images/001-consultation.png" />
              مشاوره
            </div>
          ) : (
            <div onClick={registerHandler} className={style.tab}>
              <img src="/assets/images/001-login.png" />
              ورود/ثبت نام
            </div>
          )}
          {state.loginStatus
            ? popup && <Getphone setPopUp={setPopUp} pages="support" />
            : popup && <Getphone setPopUp={setPopUp} />}

          <div onClick={carHandler} className={style.tab}>
            <img src="/assets/images/002-shopping-cart.png" alt="cartIcon" />
            سبد خرید
          </div>
        </div>
      </div>
      {/* 
      <body id="body" data-smartsupp-id="chat">
        <div id="root">
          <div className="app css-stivtw">
            <div className="MuiBox-root smart-1">
              <div
                className="MuiBox-root smart-4 css-p46klk"
                data-testid="widgetButtonWrapper"
              >
                <span className="MuiBadge-root">
                  <div className="MuiBox-root smart-5">
                    <span
                      className="MuiBadge-root"
                      data-testid="widgetButtonBadge"
                    >
                      <button
                        className="MuiButtonBase-root MuiFab-root fab css-1nv633d"
                        tabIndex="0"
                        type="button"
                        data-testid="widgetButton"
                        aria-label="Chat"
                      >
                        <span className="MuiFab-label">
                          <svg
                            className="MuiSvgIcon-root"
                            focusable="false"
                            viewBox="0 0 64 64"
                            aria-hidden="true"
                            data-testid="widgetButtonClose"
                            fontSizeAdjust="22"
                            transform="scale(-1,1)"
                            css="[object Object]"
                          >
                            <path d="M.86 18.51v-.16C3.65 7.05 19.39 3 32 3s28.39 4 31.18 15.5a66.22 66.22 0 0 1 0 20.46C62 43.7 58.13 47.59 51.82 50.37l4 5A3.47 3.47 0 0 1 53.09 61a3.39 3.39 0 0 1-1.44-.31L37.11 54c-1.79.18-3.49.27-5.07.27C21 54.31 3.63 50.23.86 38.84a60.33 60.33 0 0 1 0-20.33z"></path>
                          </svg>
                        </span>
                      </button>
                      <span className="MuiBadge-badge smart-2 MuiBadge-anchorOriginTopRightRectangle MuiBadge-invisible">
                        0
                      </span>
                    </span>
                  </div>
                  <span className="MuiBadge-badge smart-3 MuiBadge-anchorOriginBottomLeftCircular MuiBadge-dot"></span>
                </span>
              </div>
            </div>
          </div>
        </div>
      </body> */}
    </>
  );
};

export default TabBar;
