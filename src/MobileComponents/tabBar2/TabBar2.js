import Link from "next/link";
import React from "react";
import persianNumber from "../../Components/tool/persianNumber/persianNumber";
import SecondlyButton from "../../Components/tool/secondlyButton/SecondlyButton";
import style from "./TabBar2.module.css";
const TabBar2 = ({ text, coursePrice, title, offAmount, data ,buyHndler,paymentHandler,page,preLoad}) => {
  const chatOnine = () => {
    document.querySelector(".cc-unoo").click();
  };
  var handler
  if(page==="payment"){
  handler =paymentHandler
  }
  else{
    handler=buyHndler
  }
  return (
    <>

      <div className={style.tabbar2}>
        <div className={style.contactUs} onClick={chatOnine}>
          <img src="/assets/images/phone24.png" />
        </div>
        <div>
          <div>
            <span>{title}</span>
            {offAmount && (
              <span className={style.off}>
                {persianNumber(data.classes.price)}
              </span>
            )}
            <span>
              {coursePrice === "0" ? "رایگان" : persianNumber(coursePrice)}{" "}
              تومان
            </span>
          </div>
          <SecondlyButton text={text}  clickHandler={handler} preLoad={preLoad} />
        </div>
      </div>
    </>
  );
};

export default TabBar2;
