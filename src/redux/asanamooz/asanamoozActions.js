const getAllCourses = (data) => {
  return { type: "GET_ALL_COURSES", payload: data };
};

export { getAllCourses };
