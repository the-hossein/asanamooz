const initialState = {
  loading: true,
  allCourses: []
};

const asanamoozReducer = (state = initialState, action) => {
  switch (action.type) {
    case "GET_ALL_COURSES":
      return {
        allCourses: action.payload,
        loading: false
      };
    default:
      return state;
  }
};

export default asanamoozReducer;
