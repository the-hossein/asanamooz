import { useSelector } from "react-redux";

const preloadTrue = () => {
  return { type: "PRELOAD_TRUE" };
};
const getData = (data) => {
  return { type: "GET_DATA", payload: data };
};
const getphonePreloadFalse = () => {
  return { type: "GETPHONE_PRELOAD_FALSE" };
};
const getphonePreloadTrue = () => {
  return { type: "GETPHONE_PRELOAD_TRUE" };
};
const statusBuyCourse = (text) => {
  return { type: "STATUS_BUYCOURSE", payload: text };
};
const statusLoadingTrue = () => {
  return {
    type: "STATUS_LOADING_TRUE"
  };
};
const statusLoadingFalse = () => {
  return {
    type: "STATUS_LOADING_FALSE"
  };
};
const getTopics = (topic) => {
  return {
    type: "GET_TOPICS",
    payload: topic
  };
};







const addBasketUser=()=>{
 const ls=localStorage.getItem("userToken")
 const token=JSON.parse(ls).token
  return(dispatch)=>{
    dispatch(statusLoadingTrue());

    var myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${token}`);
    myHeaders.append("Content-Type", "application/json");



    const PackageDeatil = async () => {
      const deatils = await callApi(
        `${BASE_URL + packageDetails}?id=${stateLogin.userData.id}`,
        "{}",
        { "Content-Type": "application/json" },
        "GET"
      );

      dispatch(statusLoadingTrue());
      if (deatils[0].code === 200) {
        dispatch(statusLoadingTrue());

        const AddBasket = async () => {
          const raw = JSON.stringify({
            userid: userId,
            basket_title: `${stateCourse.data.title}خرید دوره`,
            phone_number: phone,
            description: `${stateCourse.data.title}خرید دوره`,
            qty: 1,
            amount: totalPrice,
            package_detail: deatils[0].data
          });

          console.log("first", raw);
          const paymentData = await callApi(
            BASE_URL + addBasket,

            raw,
            myHeaders,
            "POST"
          );
          console.log(paymentData);

          if (
            paymentData[0].code === 200 &&
            paymentData[0].data !== null
          ) {
            console.log("paymentData", stateCourse);

            let checkClass = async () => {
              let classStatus = await callApi(
                `${
                  BASE_URL + checkUserClass
                }?UserId=${userId}&ClassId=${stateLogin.userData.id}`,
                "{}",
                myHeaders,
                "GET"
              );
              if (classStatus[0].data === false) {
                router.push({
                  pathname: "/payment"
                });
              } else {
                dispatch(statusBuyCourse("bought"));
              }
            };
            checkClass();
          }
        };
        AddBasket();
      }
    };
    PackageDeatil();
  }
}





export {
  preloadTrue,
  getData,
  getphonePreloadFalse,
  getphonePreloadTrue,
  statusBuyCourse,
  statusLoadingFalse,
  statusLoadingTrue,
  getTopics
};
