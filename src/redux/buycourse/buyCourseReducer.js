const initialState = {
  preload: true,
  getphonePreload: false,
  data: [],
  statusLoading: false,
  status: "",
  topicLoading: true,
  topics: []
};

const buyCourseReducer = (state = initialState, action) => {
  switch (action.type) {
    case "PRELOAD_TRUE":
      return { ...state, preload: true };
    case "GET_DATA":
      return {
        ...state,
        preload: false,
        data: action.payload
      };
    case "GETPHONE_PRELOAD_FALSE":
      return {
        ...state,
        getphonePreload: false
      };
    case "GETPHONE_PRELOAD_TRUE":
      return {
        ...state,
        getphonePreload: true
      };
    case "STATUS_LOADING_TRUE":
      return {
        ...state,
        statusLoading: true
      };
    case "STATUS_LOADING_FALSE":
      return {
        ...state,
        statusLoading: false
      };
    case "STATUS_BUYCOURSE":
      return {
        ...state,
        status: action.payload,
        statusLoading: false
      };
    case "GET_TOPICS":
      return {
        ...state,
        topics: action.payload,
        topicLoading: false
      };
    default:
      return state;
  }
};

export default buyCourseReducer;
