import { useSelector } from "react-redux";
import { addBasket, BASE_URL, checkUserClass, packageDetails, signUp } from "../../Components/apis/urls";
import { notify } from "../../Components/tool/toast/toast";
import { getphonePreloadFalse, getphonePreloadTrue, statusBuyCourse, statusLoadingTrue } from "../buycourse/buyCourseActions";
import { alldataUser, userNotLogined } from "../login/loginActions";

const otpId = (id) => {
  return {
    type: "OTP_ID",
    payload: id
  };
};
const showPopup = () => {
  return {
    type: "SHOW_POPUP"
  };
};
const hidePopup = () => {
  return {
    type: "HIDE_POPUP"
  };
};
const statusGetPhone = (status) => {
  return {
    type: "STATUS_GETPHONE",
    payload: status
  };
};
const requestSigninTrue=()=>{
    return{
        type:"REQUEST_SIGNIN_TRUE"
    }
}
const requestSigninFalse=()=>{
    return{
        type:"REQUEST_SIGNIN_FALSE"
    }
}
const messageSignIn=(message)=>{
    return {
        type:"MESSAGE_SIGN_IN",message:message
    }
}

const signupAction=(phone,code,totalPrice)=>{
    return(dispatch)=>{
        const satatePhone=useSelector(state=>state.stateGetPhone)
  const stateLogin=useSelector(state=>state.stateLogin)
  const stateCourse=useSelector(state=>state.stateBuyCourse)
        dispatch(requestSigninTrue());

        let typeSignUp = 1;
        if (satatePhone.statusphone === "support") {
          typeSignUp = 2;
        }
    
        const signupCheck = async () => {
          var raw = JSON.stringify({
            methodname: 2,
            otpCheck: {
              phoneNumber: `${phone}`,
              code: `${code}`,
              type: typeSignUp
            }
          });
    
          const sign = await callApi(
            BASE_URL + signUp,
            raw,
            { "Content-Type": "application/json" },
            "POST"
          );
          console.log(raw);
          if (sign[0].code === 200 || sign[0].code === 201) {
         
            console.log(sign);
            dispatch(getphonePreloadTrue());
    
            dispatch(messageSignIn(sign[0].message))
            console.log(sign[0].message);
            if (sign[0].message === "با موفقیت لاگین شدید ") {
              dispatch(hidePopup());
            } else {
              notify(`${sign[0].message}`, "success");
              dispatch(hidePopup());
            }
         
            const data = {
              token: sign[0].data.token,
              exp: sign[0].data.expiration,
              phone: phone
            };
            localStorage.setItem("userToken", JSON.stringify(data));
            dispatch(alldataUser());
         
    
        dispatch(requestSigninFalse())
            dispatch(getphonePreloadFalse());
    
            if (satatePhone.statusphone === "buy") {
              dispatch(statusLoadingTrue());
    
              var myHeaders = new Headers();
              myHeaders.append("Authorization", `Bearer ${sign[0].data.token}`);
              myHeaders.append("Content-Type", "application/json");
    
                  dispatch(statusLoadingTrue());
    
                  const PackageDeatil = async () => {
                    const deatils = await callApi(
                      `${BASE_URL + packageDetails}?id=${stateCourse.data.id}`,
                      "{}",
                      { "Content-Type": "application/json" },
                      "GET"
                    );
    
                    dispatch(statusLoadingTrue());
                    if (deatils[0].code === 200) {
                      dispatch(statusLoadingTrue());
    
                      const AddBasket = async () => {
                        const raw = JSON.stringify({
                          userid: stateLogin.userData.id,
                          basket_title: `${stateCourse.data.title}خرید دوره`,
                          phone_number: phone,
                          description: `${stateCourse.data.title}خرید دوره`,
                          qty: 1,
                          amount: totalPrice,
                          package_detail: deatils[0].data
                        });
    
                        console.log("first", raw);
                        const paymentData = await callApi(
                          BASE_URL + addBasket,
    
                          raw,
                          myHeaders,
                          "POST"
                        );
                        console.log(paymentData);
    
                        if (
                          paymentData[0].code === 200 &&
                          paymentData[0].data !== null
                        ) {
                          console.log("paymentData", stateCourse);
    
                          let checkClass = async () => {
                            let classStatus = await callApi(
                              `${
                                BASE_URL + checkUserClass
                              }?UserId=${stateLogin.userData.id}&ClassId=${stateCourse.data.id}`,
                              "{}",
                              myHeaders,
                              "GET"
                            );
                            if (classStatus[0].data === false) {
                              router.push({
                                pathname: "/payment"
                              });
                            } else {
                              dispatch(statusBuyCourse("bought"));
                            }
                          };
                          checkClass();
                        }
                      };
                      AddBasket();
                    }
                  };
                  PackageDeatil();
                }
          
            
          } else {
            dispatch(messageSignIn("کد وارد شده اشتباه است"))
            dispatch(userNotLogined());
            dispatch(requestSigninFalse())

          }
      
        signupCheck();
      };

    }
}





export { otpId, showPopup, hidePopup, statusGetPhone ,signupAction};
