const initialState = {
  show: false,
  otpId: "",
  statusphone: "",
  loadingSubmit: false,
  message:""
};

const getphoneReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SHOW_POPUP":
      return {
        ...state,
        show: true
      };
    case "REQUEST_SIGNIN_TRUE":
      return {
        ...state,
        loadingSubmit: true
      };
      
      case "MESSAGE_SIGN_IN":
        return{
          ...state,message:action.message
        }
        case "REQUEST_SIGNIN_FALSE":
          return{...state,loadingSubmit:false}
    // case "SHOW":
    //   return { ...state,show: true };
    // case "HIDE":
    //   return { ...state,show: false };
    case "HIDE_POPUP":
      return {
        ...state,
        show: false
      };
    case "OTP_ID":
      return {
        ...state,
        otpId: action.payload
      };
    case "STATUS_GETPHONE":
      return {
        ...state,
        statusphone: action.payload
      };
    default:
      return state;
  }
};

export default getphoneReducer;
