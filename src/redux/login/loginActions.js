import { callApi } from "../../Components/apis/callApi";
import { BASE_URL, getBasket, getProfile } from "../../Components/apis/urls";
if (typeof window !== "undefined") {
  var ls = localStorage.getItem("userToken");
}
const userLogined = (data) => {
  return { type: "USER_LOGINED", data: data };
};

const userNotLogined = () => {
  return { type: "USER_NOTLOGINED" };
};
const getuserId = (id) => {
  return {
    type: "USER_ID",
    payload: id
  };
};
const loginTrue = () => {
  return {
    type: "LOGIN_TRUE"
  };
};
const requestGetUserData = () => {
  return {
    type: "REQUEST_GET_USERDATA"
  };
};
const SuccessUserData = (User) => {
  return {
    type: "SUCCESSUSERDATA",
    payload: User
  };
};
const basketInventory=()=>{
  return {
    type: "BASKET_INVENTORY"

  };
}
const alldataUser = () => {
  return (dispatch) => {
    dispatch(requestGetUserData());
    ls = localStorage.getItem("userToken");
    const userToken = JSON.parse(ls);
    var phone = userToken.phone;
    var token = userToken.token;
    var myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${token}`);
    myHeaders.append("Content-Type", "application/json");

    const userProfile = async () => {
      var raw = JSON.stringify({
        methodname: 3,
        loginWithPassword: {
          userName: phone
        }
      });
      const profile = await callApi(
        BASE_URL + getProfile,
        raw,
        myHeaders,
        "POST"
      );
      if (profile[0].code === 200 && profile[0].data !== null) {
        dispatch(SuccessUserData(profile[0].data));

        userToken["userid"] = profile[0].data.id;
        localStorage.setItem("userToken", JSON.stringify(userToken));
        
      }
      const GetBasket = async () => {
        const basket = await callApi(
          `${BASE_URL + getBasket}?UserId=${profile[0].data.id}`,
          "{}",
          myHeaders,
          "GET"
        );

        if (basket[0].code === 200 && basket[0].data !== null) {
          dispatch(basketInventory())
        }
      };
      GetBasket();
    };
    userProfile();
  };
};

export { userLogined, userNotLogined, getuserId, loginTrue, alldataUser,requestGetUserData,SuccessUserData,basketInventory };
