const initialState = {
  loginStatus: false,
  loadingState: false,
  phoneNumber: "",
  userData: {},
  userID: "",
  basketInventory: false
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case "REQUEST_GET_USERDATA":
      return {
        ...state,
        loadingState: true
      };
    case "SUCCESSUSERDATA":
      return {
        ...state,
        loadingState: false,
        loginStatus: true,
        userData: action.payload
      };
    case "USER_LOGINED":
      return {
        ...state,
        loginStatus: true
      };
    case "USER_NOTLOGINED":
      return { ...state, loginStatus: false };
    case "USER_ID":
      return { ...state, userID: action.id };
    case "LOGIN_TRUE":
      return {
        ...state,
        loginStatus: true
      };
    case "BASKET_INVENTORY":
      return {
        ...state,
        basketInventory: true
      };
    default:
      return state;
  }
};

export default loginReducer;
