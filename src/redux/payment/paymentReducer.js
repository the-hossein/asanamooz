const initialState = {
  loading: true,
  data: [],
  choseportLoading: true
};
const paymentReducer = (state = initialState, action) => {
  switch (action.type) {
    case "BASKET_PRELOAD_TRUE":
      return { ...state, loading: true };
    case "GET_BASKET_DATA":
      return {
        loading: false,
        data: action.payload
      };
    case "CHOSE_PORT_FALSE":
      return { ...state, choseportLoading: false };
    default:
      return state;
  }
};

export default paymentReducer;
