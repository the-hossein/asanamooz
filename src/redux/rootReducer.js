import { combineReducers } from "redux";
import asanamoozReducer from "./asanamooz/asanamoozReducer";
import buyCourseReducer from "./buycourse/buyCourseReducer";
import getphoneReducer from "./getphone/getphoneReducer";

import loginReducer from "./login/loginReducer";
import paymentReducer from "./payment/paymentReducer";
import userClassesReducer from "./userClasses/userClassesReducer";

const rootReducer = combineReducers({
  stateBuyCourse: buyCourseReducer,
  stateGetPhone: getphoneReducer,
  stateLogin: loginReducer,
  stateAsanAmoz: asanamoozReducer,
  statePayment: paymentReducer,
  stateUserClass: userClassesReducer,

});

export default rootReducer;
