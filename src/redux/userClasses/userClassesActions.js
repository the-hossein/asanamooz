const getUserClasses = (data) => {
  return { type: "GET_USER_CLASSES", payload: data };
};
export { getUserClasses };
