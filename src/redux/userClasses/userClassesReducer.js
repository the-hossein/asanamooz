import React from "react";
const initializedState = {
  loading: true,
  data: []
};
const userClassesReducer = (state = initializedState, action) => {
  switch (action.type) {
    case "GET_USER_CLASSES":
      return {
        loading: false,
        data: action.payload
      };

    default:
      return state;

  }
};

export default userClassesReducer;
